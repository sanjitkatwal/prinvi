<?php

return [
    'product'   =>[
        'image-dimentions' =>[
            'main-image'=>[
                [
                    'height'=>300,
                    'width'=>300,
                ],
                [
                    'height'=>150,
                    'width'=>150,
                ],
            ],

            'gallery-image'=>[
                [
                    'height'=>200,
                    'width'=>200,
                ],
                [
                    'height'=>600,
                    'width'=>600,
                ]
            ]
        ]
    ]
];
