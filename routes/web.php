<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin/', 'as' => 'admin.', 'namespace' => 'Admin\\'], function () {

    Route::get('profile',                     ['as' => 'profile',                   'uses' => 'UserController@profile']);
    Route::post('profile',                    ['as' => 'profile.update',            'uses' => 'UserController@profileUpdate']);
    Route::get('dashboard',                   ['as' => 'dashboard',                 'uses' => 'DashboardController@index']);

    //Route::resource('user','userController');
    Route::get('user',                        ['as' => 'user',                      'uses' => 'UserController@index']);
    Route::get('user/show/{id}',              ['as' => 'user.show',                 'uses' => 'UserController@show']);
    Route::get('user/create',                 ['as' => 'user.create',               'uses' => 'UserController@create']);
    Route::post('user/store',                 ['as' => 'user.store',                'uses' => 'UserController@store']);
    Route::get('user/edit/{id}',              ['as' => 'user.edit',                 'uses' => 'UserController@edit']);
    Route::post('user/update/{id}',           ['as' => 'user.update',               'uses' => 'UserController@update']);
    Route::get('user/delete/{id}',            ['as' => 'user.delete',               'uses' => 'UserController@destroy']);


    //This is test controller
    Route::get('test',                        ['as' => 'test',                      'uses' => 'TestController@index']);
    Route::get('test/show/{id}',              ['as' => 'test.show',                 'uses' => 'TestController@show']);
    Route::get('test/create',                 ['as' => 'test.create',               'uses' => 'TestController@create']);
    Route::post('test/store',                 ['as' => 'test.store',                'uses' => 'TestController@store']);
    Route::get('test/edit/{id}',              ['as' => 'test.edit',                 'uses' => 'TestController@edit']);
    Route::post('test/update/{id}',           ['as' => 'test.update',               'uses' => 'TestController@update']);
    Route::get('test/delete/{id}',            ['as' => 'test.delete',               'uses' => 'TestController@destroy']);

    // Route::resource('tags', 'TagsController');
    Route::get('tag',                         ['as' => 'tag',                       'uses' => 'TagController@index']);
    Route::get('tag/show/{id}',               ['as' => 'tag.show',                  'uses' => 'TagController@show']);
    Route::get('tag/create',                  ['as' => 'tag.create',                'uses' => 'TagController@create']);
    Route::post('tag/store',                  ['as' => 'tag.store',                 'uses' => 'TagController@store']);
    Route::get('tag/edit/{id}',               ['as' => 'tag.edit',                  'uses' => 'TagController@edit']);
    Route::post('tag/update/{id}',            ['as' => 'tag.update',                'uses' => 'TagController@update']);
    Route::get('tag/delete/{id}',             ['as' => 'tag.delete',                'uses' => 'TagController@destroy']);

    Route::get('brand',                       ['as' => 'brand',                     'uses' => 'BrandController@index']);
    Route::get('brand/show/{id}',             ['as' => 'brand.show',                'uses' => 'BrandController@show']);
    Route::get('brand/create',                ['as' => 'brand.create',              'uses' => 'BrandController@create']);
    Route::post('brand/store',                ['as' => 'brand.store',               'uses' => 'BrandController@store']);
    Route::get('brand/edit/{id}',             ['as' => 'brand.edit',                'uses' => 'BrandController@edit']);
    Route::post('brand/update/{id}',          ['as' => 'brand.update',              'uses' => 'BrandController@update']);
    Route::get('brand/delete/{id}',           ['as' => 'brand.delete',              'uses' => 'BrandController@destroy']);

    // Route::resource('categories', 'CategoriesController');
    Route::get('category',                    ['as' => 'category',                  'uses' => 'CategoryController@index']);
    Route::get('category/show/{id}',          ['as' => 'category.show',             'uses' => 'CategoryController@show']);
    Route::get('category/create',             ['as' => 'category.create',           'uses' => 'CategoryController@create']);
    Route::post('category/store',             ['as' => 'category.store',            'uses' => 'CategoryController@store']);
    Route::get('category/edit/{id}',          ['as' => 'category.edit',             'uses' => 'CategoryController@edit']);
    Route::post('category/update/{id}',       ['as' => 'category.update',           'uses' => 'CategoryController@update']);
    Route::get('category/delete/{id}',        ['as' => 'category.delete',           'uses' => 'CategoryController@destroy']);

    Route::get('slider',                      ['as' => 'slider',                    'uses' => 'SliderController@index']);
    Route::get('slider/show/{id}',            ['as' => 'slider.show',               'uses' => 'SliderController@show']);
    Route::get('slider/create',               ['as' => 'slider.create',             'uses' => 'SliderController@create']);
    Route::post('slider/store',               ['as' => 'slider.store',              'uses' => 'SliderController@store']);
    Route::get('slider/edit/{id}',            ['as' => 'slider.edit',               'uses' => 'SliderController@edit']);
    Route::post('slider/update/{id}',         ['as' => 'slider.update',             'uses' => 'SliderController@update']);
    Route::get('slider/delete/{id}',          ['as' => 'slider.delete',             'uses' => 'SliderController@destroy']);

    Route::get('page',                        ['as' => 'page',                      'uses' => 'PageController@index']);
    Route::get('page/show/{id}',              ['as' => 'page.show',                 'uses' => 'PageController@show']);
    Route::get('page/create',                 ['as' => 'page.create',               'uses' => 'PageController@create']);
    Route::post('page/store',                 ['as' => 'page.store',                'uses' => 'PageController@store']);
    Route::get('page/edit/{id}',              ['as' => 'page.edit',                 'uses' => 'PageController@edit']);
    Route::post('page/update/{id}',           ['as' => 'page.update',               'uses' => 'PageController@update']);
    Route::get('page/delete/{id}',            ['as' => 'page.delete',               'uses' => 'PageController@destroy']);

    Route::get('menu-section',                ['as' => 'menu-section',              'uses' => 'MenuSectionController@index']);
    Route::get('menu-section/edit/{id}',      ['as' => 'menu-section.edit',         'uses' => 'MenuSectionController@edit']);
    Route::post('menu-section/update/{id}',   ['as' => 'menu-section.update',       'uses' => 'MenuSectionController@update']);
    Route::post('menu-section/load-page-row', ['as' => 'menu-section.load-page-row','uses' => 'MenuSectionController@loadPageRow']);

    Route::get('currency',                    ['as' => 'currency',                  'uses' => 'CurrencyController@index']);
    Route::get('currency/create',             ['as' => 'currency.create',           'uses' => 'CurrencyController@create']);
    Route::post('currency/store',             ['as' => 'currency.store',            'uses' => 'CurrencyController@store']);
    Route::get('currency/edit/{id}',          ['as' => 'currency.edit',             'uses' => 'CurrencyController@edit']);
    Route::post('currency/update/{id}',       ['as' => 'currency.update',           'uses' => 'CurrencyController@update']);
    Route::get('currency/delete/{id}',        ['as' => 'currency.delete',           'uses' => 'CurrencyController@destroy']);
    Route::post('currency/reorder-currency',  ['as' => 'currency.reorder-currency', 'uses' => 'CurrencyController@reorderCurrency']);

    Route::get('attribute',                   ['as' => 'attribute',                 'uses' => 'AttributeController@index']);
    Route::get('attribute/show/{id}',         ['as' => 'attribute.show',            'uses' => 'AttributeController@show']);
    Route::get('attribute/create',            ['as' => 'attribute.create',          'uses' => 'AttributeController@create']);
    Route::post('attribute/store',            ['as' => 'attribute.store',           'uses' => 'AttributeController@store']);
    Route::get('attribute/edit/{id}',         ['as' => 'attribute.edit',            'uses' => 'AttributeController@edit']);
    Route::post('attribute/update/{id}',      ['as' => 'attribute.update',          'uses' => 'AttributeController@update']);
    Route::get('attribute/delete/{id}',       ['as' => 'attribute.delete',          'uses' => 'AttributeController@destroy']);

    Route::get('attribute-group',             ['as' => 'attribute-group',           'uses' => 'AttributeGroupController@index']);
    Route::get('attribute-group/show/{id}',   ['as' => 'attribute-group.show',      'uses' => 'AttributeGroupController@show']);
    Route::get('attribute-group/create',      ['as' => 'attribute-group.create',    'uses' => 'AttributeGroupController@create']);
    Route::post('attribute-group/store',      ['as' => 'attribute-group.store',     'uses' => 'AttributeGroupController@store']);
    Route::get('attribute-group/edit/{id}',   ['as' => 'attribute-group.edit',      'uses' => 'AttributeGroupController@edit']);
    Route::post('attribute-group/update/{id}',['as' => 'attribute-group.update',    'uses' => 'AttributeGroupController@update']);
    Route::get('attribute-group/delete/{id}', ['as' => 'attribute-group.delete',    'uses' => 'AttributeGroupController@destroy']);

    Route::get('product',                         ['as' => 'product',                        'uses' => 'ProductController@index']);
    Route::get('product/create',                  ['as' => 'product.create',                 'uses' => 'ProductController@create']);
    Route::post('product/store',                  ['as' => 'product.store',                  'uses' => 'ProductController@store']);
    Route::get('product/show/{id}',               ['as' => 'product.show',                   'uses' => 'ProductController@show']);
    Route::get('product/edit/{id}',               ['as' => 'product.edit',                   'uses' => 'ProductController@edit']);
    Route::post('product/update/{id}',            ['as' => 'product.update',                 'uses' => 'ProductController@update']);
    Route::get('product/delete/{id}',             ['as' => 'product.delete',                 'uses' => 'ProductController@destroy']);
    Route::post('product/load-attribute-html',    ['as' => 'product.load-attribute-row',     'uses' => 'ProductController@loadAttributeRow']);
    Route::post('product/load-attribute-by-group',['as' => 'product.load-attribute-by-group','uses' => 'ProductController@loadAttributeByGroup']);
    Route::post('product/load-gallery-html',      ['as' => 'product.load-gallery-row',       'uses' => 'ProductController@loadGalleryRow']);
    Route::post('product/load-slug',      ['as' => 'product.load-slug',              'uses' => 'ProductController@loadSlug']);

});


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');
Route::get('product/{slug}', ['as' => 'product',      'uses'=> 'HomeController@index']);
