<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title', 150);
            $table->string('slug', 150)->unique();
            $table->string('main_image', 255);
            $table->double('old_price', 10, 2);
            $table->double('new_price', 10, 2);
            $table->unsignedInteger('quantity');
            $table->text('short_desc')->nullable();
            $table->text('long_desc')->nullable();
            $table->string('seo_title', 255)->nullable();
            $table->text('seo_description', 255)->nullable();
            $table->string('seo_keywords', 255)->nullable();
            $table->unsignedInteger('hits')->default(0);
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('brand_id');
            $table->boolean('status')->dufault(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
