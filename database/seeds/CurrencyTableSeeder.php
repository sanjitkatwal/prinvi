<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            'created_at'        =>Carbon::now(),
            'updated_at'        =>Carbon::now(),
            'is_default'        =>1,
            'title'             =>'USD',
            'slug'              =>'usd',
            'symbol'            =>'$',
            'rate'              =>1,
            'status'            =>1,
        ]);
    }
}
