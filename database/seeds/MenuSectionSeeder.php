<?php

use Illuminate\Database\Seeder;

class MenuSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('menu_section')->insert([
            'title'     => 'Top Navigation',
            'slug'     => 'top-navigation',
            'hint'     => 'This is for upper top header section.',
        ]);

        \DB::table('menu_section')->insert([
            'title'     => 'Navigation',
            'slug'     => 'nav-bar',
            'hint'     => 'This is for side bar section.',
        ]);

        \DB::table('menu_section')->insert([
            'title'     => 'Footer 1',
            'slug'     => 'footer-1',
            'hint'     => null,
        ]);

        \DB::table('menu_section')->insert([
            'title'     => 'Footer 2',
            'slug'     => 'footer-2',
            'hint'     => null,
        ]);

        \DB::table('menu_section')->insert([
            'title'     => 'Footer 3',
            'slug'     => 'footer-3',
            'hint'     => null,
        ]);

        \DB::table('menu_section')->insert([
            'title'     => 'Footer 4',
            'slug'     => 'footer-4',
            'hint'     => null,
        ]);

        \DB::table('menu_section')->insert([
            'title'     => 'Footer 5',
            'slug'     => 'footer-5',
            'hint'     => null,
        ]);

        \DB::table('menu_section')->insert([
            'title'     => 'Footer 6',
            'slug'     => 'footer-6',
            'hint'     => null,
        ]);
    }
}
