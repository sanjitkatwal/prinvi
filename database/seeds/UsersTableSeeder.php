<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();

        $user->name = 'admin';
        $user->first_name = 'sanjit';
        $user->last_name = 'katwal';
        $user->email = 'admin@gmail.com';
        $user->password = bcrypt('admin@123');

        $user->save();
    }
}
