@extends('admin.layouts.app')
@section('page_titel')
    Profile:Show
@endsection

@section('content')

    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                @include('admin.includes.dashboard_link')

                <li>
                    <a href="{{ route($_base_route) }}">{{ $_panel }}</a>
                </li>
                <li class="active">Show</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">

            <div class="page-header">
                <h1>
                    {{ $_panel }} Manager
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Show
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="simple-table" class="table  table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 20%;">Column Name</th>
                                    <th>Value</th>
                                </tr>
                                </thead>
                                <tr>
                                    <td>Id</td>
                                    <td>{!! $data['row']->id !!}</td>
                                </tr>
                                <tr>
                                    <td>Title</td>
                                    <td>{!! $data['row']->title !!}</td>
                                </tr>

                                <tr>
                                    <td>Quantity</td>
                                    <td>{!! $data['row']->quantity !!}</td>
                                </tr>
                                <tr>
                                    <td>Category</td>
                                    <td>
                                        {{--@if($data['row']->parent_id == 0)
                                            <p>Parent Category</p>

                                            @else
                                            <p>{{ \App\Models\Category::find($data['row']->parent_id)->title }}</p>
                                        @endif--}}
                                        {!! $data['row']->category->title !!}

                                    </td>
                                </tr>
                                <tr>
                                    <td>Url</td>
                                    <td><a target="_blank" href="{{ route('product', $data['row']->slug) }}"> {{ route('product', $data['row']->slug) }} </a> </td>
                                </tr>
                                <tr>
                                    <td>Image</td>
                                    <td>
                                        @if($data['row']->main_image)
                                            <img src="{{ asset('images/'.$_folder.'/'.$data['row']->main_image) }}" style="width: 80px; height: 60px;">
                                        @else
                                            <p>No Image</p>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Brand</td>
                                    <td>{!! $data['row']->brand->title !!}</td>
                                </tr>
                                <tr>
                                    <td>Price</td>
                                    <td>{!! $data['currency']->symbol. ''. $data['row']->new_price. ' '. '<del>'. $data['currency']->symbol. ''. $data['row']->old_price .'</del>' !!}</td>
                                </tr>
                                <tr>
                                    <td>Tag</td>
                                    <td>
                                        @foreach($data['row']->tags as $tag)
                                            <button class="btn btn-xs btn-primary">{{ '#'.$tag->title }}</button>
                                            @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Short Description</td>
                                    <td>{!! $data['row']->short_desc !!}</td>
                                </tr>

                                <tr>
                                    <td>Long Description</td>
                                    <td>{!! $data['row']->long_desc !!}</td>
                                </tr>

                                <tr>
                                    <td>Status</td>
                                    <td>
                                        @if($data['row']->status == 1)
                                            <button class="btn btn-xs btn-success">Active</button>
                                            @else
                                            <button class="btn btn-xs btn-warning">In-active</button>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>View Count</td>
                                    <td>{!! $data['row']->hits !!}</td>
                                </tr>
                                <tr>
                                    <td>Created At</td>
                                    <td>{!! $data['row']->created_at !!}</td>
                                </tr>

                                <tr>
                                    <td>Updated At</td>
                                    <td>{!! isset($data['row']->updated_at)?$data['row']->updated_at->diffForHumans():'' !!}</td>
                                </tr>

                                <tr>
                                    <td>Attributes</td>
                                    <td>
                                        <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Attribute Group</th>
                                                <th>Attribute</th>
                                                <th>Value</th>
                                            </tr>
                                            </thead>

                                            <tbody id="row_wrapper" class="ui-sortable">
                                            @foreach($data['attributes'] as $attribute)
                                                <tr class="ui-sortable-handle">
                                                    <td>
                                                        <p>{{ $attribute->group_title }}</p>

                                                    </td>
                                                    <td>
                                                        {{ $attribute->attribute_title }}
                                                    </td>
                                                    <td width="50%">
                                                        <p>{{ $attribute->content }}</p>
                                                    </td>
                                                </tr>

                                                @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Gallery</td>
                                    <td>
                                        <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Alternative Text</th>
                                                <th>Caption</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>

                                            <tbody id="row_wrapper" class="ui-sortable">
                                            @foreach($data['gallery_images'] as $images)
                                                <tr class="ui-sortable-handle">
                                                    <td>
                                                        <img src="{{ asset('images/'.$_folder.'/'.$images->image) }}" style="max-width: 100px;">
                                                    </td>
                                                    <td>
                                                        {{ $images->alt_text }}
                                                    </td>
                                                    <td width="50%">
                                                        <p>{{ $images->caption }}</p>
                                                    </td>
                                                    <td>
                                                        @if($images->status == 1)
                                                            <button class="btn btn-xs btn-primary">Active</button>
                                                            @else
                                                            <button class="btn btn-xs btn-warning">In-active</button>
                                                        @endif
                                                    </td>
                                                </tr>

                                            @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- /.span -->
                    </div><!-- /.row -->

                    <div class="hr hr-18 dotted hr-double"></div>

                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

    @endsection
