@extends('admin.layouts.app')
@section('page_titel')
    {{ $_panel }}:create
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('admin/assets/css/chosen.min.css') }}" />
    @endsection


@section('content')

    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">

               @include('admin.includes.dashboard_link')

                <li>
                    <a href="{{ route($_base_route) }}">{{ $_panel }}</a>
                </li>
                <li class="active">Create </li>
            </ul><!-- /.breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
                    <span class="input-icon">
                        <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
                        <i class="ace-icon fa fa-search nav-search-icon"></i>
                    </span>
                </form>
            </div><!-- /.nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $_panel }} Manager
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Create Form
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">

                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    {!! Form::open([
                    'url'       => route($_base_route.'.store'),
                    'class'     => 'form-horizontal',
                    'id'        =>'validation-form',
                    'enctype'   => 'multipart/form-data'
                    ]) !!}

                    @include($_view_path.'.common.form', ['button'=>'Save'])


                    {!! Form::close() !!}

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
@endsection

@section('page_specific_script')
    <script src="{{ asset('admin/assets/js/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/jquery.validate.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#validation-form').validate();
        });
    </script>
    <script>
        $(document).ready(function () {

            $(".chosen-select").chosen();


            //to add the new row while click on the + sign in attribute
            $('#html-loader-btn').click(function () {
                $.ajax({
                    method: 'POST',
                    url:    '{{ route('admin.product.load-attribute-row') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                    },
                    success:function (response) {

                        var data = $.parseJSON(response);
                        $('#row_wrapper').append(data.html);
                    }
                });
            });

            $('body').on('click', '.attribute_group', function () {
                var $this = $(this);
                $.ajax({
                    method: 'POST',
                    url:    '{{ route('admin.product.load-attribute-by-group') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        attribute_group_id: $this.val()
                    },
                    success:function (response) {

                        var data = $.parseJSON(response);
                        $this.closest('tr').find('.attribute').html(data.html);
                    }
                });
            });

            //to add the new row while click on the + sign in gallery
            $('#gallery-html-loader-btn').click(function () {
                $.ajax({
                    method: 'POST',
                    url:    '{{ route('admin.product.load-gallery-row') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                    },
                    success:function (response) {

                        var data = $.parseJSON(response);
                        $('#gallery_row_wrapper').append(data.html);
                    }
                });
            });

            //this is for the slug
            $('input[name = "title"]').bind("keyup blur", function () {

                loadSlugForTitle($(this))
            });

            function loadSlugForTitle($this) {
                $.ajax({
                    method: 'POST',
                    url:    '{{ route('admin.product.load-slug') }}',
                    data: {
                        _token: '{{ csrf_token() }}',
                        title: $this.val(),
                    },
                    success:function (response) {
                        var data = $.parseJSON(response);

                        if(data.error){
                            $('#dublicate-title-notification').html(data.message);
                        }else{
                            $('input[name="slug"]').val(data.slug);
                            $('#dublicate-title-notification').html('');
                        }
                    }
                });
            }

        });
    </script>

    @include('admin.layouts.common.jquery_sortable_script')

@endsection

