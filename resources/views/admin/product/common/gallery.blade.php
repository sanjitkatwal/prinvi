<div class="table-responsive">
    <table id="sample-table-1" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>Sort</th>
            <th>Choose Image</th>
            <th>Alt Text</th>
            <th>Caption</th>
            <th>Status</th>
            <th>
                <button type="button" class="btn btn-sm btn-primary" id="gallery-html-loader-btn">+</button>
            </th>
        </tr>
        </thead>

        <tbody id="gallery_row_wrapper" class="ui-sortable">
        @if(!isset($data['row']))
            @include($_view_path.'.common.gallery_row')
        @else
            @include($_view_path.'.common.gallery_row_edit')
        @endif
        </tbody>
    </table>
</div>
