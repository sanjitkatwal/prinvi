<div class="table-responsive">
    <table id="sample-table-1" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>Choose Attribute Group</th>
            <th>Choose Attribute</th>
            <th>Value</th>
            <th>
                <button type="button" class="btn btn-sm btn-primary" id="html-loader-btn">+</button>
            </th>
        </tr>
        </thead>

        <tbody id="row_wrapper" class="ui-sortable">
            @if(!isset($data['row']))
                @include($_view_path.'.common.attribute_row')
            @else

                @foreach($data['attributes'] as $attribute)

                    @include($_view_path.'.common.attribute_row_edit')

                @endforeach

            @endif
        </tbody>
    </table>
</div>
