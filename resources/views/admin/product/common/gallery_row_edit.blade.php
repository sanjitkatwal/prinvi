@foreach($data['row']->images as $image)

    <tr class="ui-sortable-handle">
        <td><i class="icon-move bigger-120"></i></td>
        <td width="15%">
            <img src="{{ asset('images/'.$_folder.'/'.$image->image) }}" alt="" style="max-width: 200px;">
            <input type="hidden" name="gallery_id[]" value="{{ $image->id }}">
            {!! Form::file('gallery_image[]')!!}
        </td>
        <td width="25%">
            {!! Form::text('gallery_alt_text[]', $image->alt_text, ['class' => 'form-control']) !!}
        </td>
        <td>
            {!! Form::text('gallery_caption[]', $image->caption, ['class' => 'form-control']) !!}
        </td>
        <td>
            {!! Form::select('gallery_status[]', ['1' => 'Active', '0' => 'In-active'], $image->status, ['class'=>'form-control']) !!}
        </td>
        <td>
            <button type="button" class="btn btn-sm btn-danger page-row-remove-btn" onclick="(this).closest('tr').remove();">X</button>
        </td>
    </tr>

@endforeach
