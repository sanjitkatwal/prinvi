<div class="tabbable">
    <ul class="nav nav-tabs" id="myTab">
        <li class="active">
            <a data-toggle="tab" href="#general">
                <i class="green ace-icon fa fa-home bigger-120"></i>
                General
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#attributes">
                Attributes
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#gallery">
                Gallery
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#seo">
                SEO
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="general" class="tab-pane fade in active">

           @include($_view_path.'.common.general')
        </div>

        <div id="attributes" class="tab-pane fade">
           @include($_view_path.'.common.attributes')
        </div>

        <div id="gallery" class="tab-pane fade">
            @include($_view_path.'.common.gallery')
        </div>

        <div id="seo" class="tab-pane fade">
            @include($_view_path.'.common.seo')
        </div>

    </div>
</div>



<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">

        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            {{ $button }}
        </button>

        &nbsp; &nbsp;&nbsp;
        {!!
         Form::button('<i class="ace-icon fa fa-undo bigger-110">&nbsp; Reset</i>', [
            'class' =>'btn',
            'type'  => 'reset'
         ])
         !!}
    </div>
</div>
<div class="space-4"></div>

