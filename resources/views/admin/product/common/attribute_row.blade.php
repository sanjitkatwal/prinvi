<tr class="ui-sortable-handle">
    <td>
        {!! Form::select('attribute_group[]' , $data['attribute_groups']->pluck('title', 'id'), null, [
            'class' => 'form-control attribute_group'
        ]) !!}
    </td>
    <td>
        {!! Form::select('attribute[]' , $data['attributes']->pluck('title', 'id'), null, [
            'class' => 'form-control attribute'
        ]) !!}
    </td>
    <td width="50%">
        {!! Form::text('attribute_value[]', null, ['class'=>'form-control']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-sm btn-danger page-row-remove-btn" onclick="(this).closest('tr').remove();">X</button>
    </td>
</tr>
