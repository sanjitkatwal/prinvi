<tr class="ui-sortable-handle">
    <td>
        {!! Form::select('attribute_group[]' , $data['attribute_groups']->pluck('title', 'id'), $attribute->attribute_group_id, [
            'class' => 'form-control attribute_group'
        ]) !!}
    </td>
    <td>
        {!! Form::select('attribute[]' , $attribute->attribute_list->pluck('title', 'id'), $attribute->attribute_id, [
            'class' => 'form-control attribute'
        ]) !!}
    </td>
    <td width="50%">
        {!! Form::text('attribute_value[]', $attribute->content, ['class'=>'form-control']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-sm btn-danger page-row-remove-btn" onclick="(this).closest('tr').remove();">X</button>
    </td>
</tr>
