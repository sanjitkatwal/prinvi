<div class="form-group">
    {!! Form::label('seo_title', 'SEO Title', [
    'class' => 'col-sm-3 control-label no-padding-right',
    ])
     !!}
    <div class="col-sm-9">
        {!! Form::text('seo_title', null, [
            'class'         => 'col-xs-10 col-sm-5',
            'placeholder'   => 'SEO Title',
        ]) !!}
        @if ($errors->has('seo_title'))
            <span class="help-block">
                <strong>{{ $errors->first('seo_title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('seo_keywords')?'has-error':'' }} " for="description">SEO Key-Words</label>
    <div class="col-sm-9">
        {!! Form::textarea('seo_keywords', null, [
        'class'     => 'form-control',
        'placeholder' => 'SEO Key-Words',
        'cols'      => 60,
        'rows'      => 7,
        ]) !!}
        @if ($errors->has('seo_keywords'))
            <span class="help-block">
                <strong>{{ $errors->first('seo_keywords') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('seo_description')?'has-error':'' }} " for="description">SEO Description</label>

    <div class="col-sm-9">
        {!! Form::textarea('seo_description', null, [
        'class'     => 'form-control',
        'placeholder' => 'SEO Description',
        'cols'      => 60,
        'rows'      => 7,
        ]) !!}
        @if ($errors->has('seo_description'))
            <span class="help-block">
                <strong>{{ $errors->first('seo_description') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>
