<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('category_id')?'has-error':'' }}" for="name">Categories </label>

    <div class="col-sm-9">
        {!!
            Form::select('category_id', $data['categories']->pluck('title', 'id'), null, [
            'class'     => 'col-xs-10 col-sm-5','required'
            ])
        !!}
        @if ($errors->has('category_id'))
            <span class="help-block">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    {!! Form::label('title', 'Title', [
    'class' => 'col-sm-3 control-label no-padding-right'
    ])
     !!}
    <div class="col-sm-9">
        {!! Form::text('title', null, [
            'class'         => 'col-xs-10 col-sm-5','required',
            'placeholder'   => 'Title',
        ]) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
        <span class="help-block" id="dublicate-title-notification"></span>
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    {!! Form::label('url', 'Url', [
    'class' => 'col-sm-3 control-label no-padding-right',
    ])
     !!}
    <div class="col-sm-9">
        {!! Form::text('slug', null, [
            'class'         => 'col-xs-10 col-sm-5','required',
            'placeholder'   => 'URL',
        ]) !!}
        @if ($errors->has('slug'))
            <span class="help-block">
                <strong>{{ $errors->first('slug') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    {!! Form::label('new price', 'New Price', [
    'class' => 'col-sm-3 control-label no-padding-right',
    ])
     !!}
    <div class="col-sm-9">
        {!! Form::number('new_price', null, [
            'class'         => 'col-xs-10 col-sm-5','required',
            'placeholder'   => 'New Price',
            'min'           => 1
        ]) !!}
        @if ($errors->has('new_price'))
            <span class="help-block">
                <strong>{{ $errors->first('new_price') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" {{ $errors->has('old_price')? 'old_price':'' }} for="old_price">Old Price</label>
    <div class="col-sm-9">
        {!! Form::number('old_price', null, [
            'class'         => 'col-xs-10 col-sm-5',
            'placeholder'   => 'Old Price',
            'min'           => 1
        ]) !!}
        @if ($errors->has('old_price'))
            <span class="help-block">
                <strong>{{ $errors->first('old_price') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" {{ $errors->has('quantity')? 'quantity':'' }} for="quantity">Quantity</label>
    <div class="col-sm-9">
        {!! Form::text('quantity', null, [
            'class'         => 'col-xs-10 col-sm-5','required',
            'placeholder'   => 'Quantity',
        ]) !!}
        @if ($errors->has('quantity'))
            <span class="help-block">
                <strong>{{ $errors->first('quantity') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('brand_id')?'has-error':'' }}" for="name">Brand </label>

    <div class="col-sm-9">
        {!!
            Form::select('brand_id', $data['brands']->pluck('title', 'id'), null, [
            'class'     => 'col-xs-10 col-sm-5','required'
            ])
        !!}
        @if ($errors->has('brand_id'))
            <span class="help-block">
                <strong>{{ $errors->first('brand_id') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('tags')?'has-error':'' }}" for="name">Tags </label>

    <div class="col-sm-9">
        {!!
            Form::select('tags[]', $data['tags']->pluck('title', 'id'), null, [
            'class'     => 'col-xs-10 col-sm-5 chosen-select','required',
            'multiple'
            ])
        !!}
        @if ($errors->has('tags'))
            <span class="help-block">
                <strong>{{ $errors->first('tags') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

@if(isset($data['row']))
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="main_image">Existing Image</label>

        <div class="col-sm-9">
            @if($data['row']->main_image)
                <img src="{{ asset('images/'.$_folder.'/'.$data['row']->main_image) }}" style="width: 80px; height: 60px;">
                <label for="checkbok">
                    <input type="checkbox" name="remove_image">
                    <span>Remove Image</span>
                </label>
            @else
                <p>No Image</p>
            @endif
        </div>
    </div>
    <div class="space-4"></div>

@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('main_image')?'has-error':'' }}" for="banner_image">Image</label>

    <div class="col-sm-9">
        {!! Form::file('image', ['class'=>'col-xs-10 col-sm-5']) !!}
        @if ($errors->has('image'))
            <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('short_desc')?'has-error':'' }} " for="description">Short Description</label>

    <div class="col-sm-9">
        {!! Form::textarea('short_desc', null, [
        'class'     => 'form-control','required',
        'placeholder' => 'Description',
        'cols'      => 60,
        'rows'      => 7,
        ]) !!}
        @if ($errors->has('short_desc'))
            <span class="help-block">
                <strong>{{ $errors->first('short_desc') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('long_desc')?'has-error':'' }} " for="description">Long Description</label>

    <div class="col-sm-9">
        {!! Form::textarea('long_desc', null, [
        'class'     => 'form-control',
        'placeholder' => 'Description',
        'cols'      => 60,
        'rows'      => 7,
        ]) !!}
        @if ($errors->has('long_desc'))
            <span class="help-block">
                <strong>{{ $errors->first('long_desc') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('status')?'has-error':'' }} " for="status">Status</label>

    <div class="col-sm-9">
        {!!
            Form::select('status', ['' => 'Select Status', '1' => 'Active', '0' => 'In-Active'])
        !!}
    </div>
</div>
<div class="space-4"></div>
