<tr class="ui-sortable-handle">
    <td><i class="icon-move bigger-120"></i></td>
    <td width="15%">
        <input type="hidden" name="gallery_id[]" value="">
        {!! Form::file('gallery_image[]')!!}
    </td>
    <td width="25%">
        {!! Form::text('gallery_alt_text[]', null, ['class' => 'form-control']) !!}
    </td>
    <td>
        {!! Form::text('gallery_caption[]', null, ['class' => 'form-control']) !!}
    </td>
    <td>
        {!! Form::select('gallery_status[]', ['1' => 'Active', '0' => 'In-active'], 1, ['class'=>'form-control']) !!}
    </td>
    <td>
        <button type="button" class="btn btn-sm btn-danger page-row-remove-btn" onclick="(this).closest('tr').remove();">X</button>
    </td>
</tr>
