<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
    <script type="text/javascript">
        try{ace.settings.loadState('sidebar')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="ace-icon fa fa-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="ace-icon fa fa-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="ace-icon fa fa-users"></i>
            </button>

            <button class="btn btn-danger">
                <i class="ace-icon fa fa-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li class="active">
            <a href="{{ route('admin.dashboard') }}">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
        </li>

        <li {!! request()->is('admin/user*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> User Manager </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li {!! request()->is('admin/user*')?'class="active"':'' !!}>
                    <a href="{{ route('admin.user') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List User
                    </a>

                    <b class="arrow"></b>
                </li>

                <li {!! request()->is('admin/user/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.user.create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add User
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/test*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Test Manager </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li {!! request()->is('admin/test*')?'class="active"':'' !!}>
                    <a href="{{ route('admin.test') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List Test Category
                    </a>

                    <b class="arrow"></b>
                </li>

                <li {!! request()->is('admin/test/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.test.create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add Test Category
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/brand*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Brand Manager </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li {!! request()->is('admin/brand')?'class="active"':'' !!}>
                    <a href="{{ route('admin.brand') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List Brand
                    </a>

                    <b class="arrow"></b>
                </li>

                <li {!! request()->is('admin/brand/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.brand.create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add Brand
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/tag*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Tag Manager </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li {!! request()->is('admin/tag')?'class="active"':'' !!}>
                    <a href="{{ route('admin.tag') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List Tag
                    </a>

                    <b class="arrow"></b>
                </li>

                <li {!! request()->is('admin/tag/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.tag.create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add Tag
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/product*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Product Manager </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li {!! request()->is('admin/product')?'class="active"':'' !!}>
                    <a href="{{ route('admin.product') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List Product
                    </a>

                    <b class="arrow"></b>
                </li>

                <li {!! request()->is('admin/product/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.product.create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add Product
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>


        <li {!! request()->is('admin/category*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Category Manager </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li {!! request()->is('admin/category')?'class="active"':'' !!}>
                    <a href="{{ route('admin.category') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List Category
                    </a>

                    <b class="arrow"></b>
                </li>

                <li {!! request()->is('admin/category/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.category.create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add Category
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/slider*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Slider Manager </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li {!! request()->is('admin/slider')?'class="active"':'' !!}>
                    <a href="{{ route('admin.slider') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List Slider
                    </a>

                    <b class="arrow"></b>
                </li>

                <li {!! request()->is('admin/slider/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.slider.create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add Slider
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/page*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Page Manager </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li {!! request()->is('admin/page')?'class="active"':'' !!}>
                    <a href="{{ route('admin.page') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List Page
                    </a>

                    <b class="arrow"></b>
                </li>

                <li {!! request()->is('admin/page/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.page.create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add Page
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/menu-section')?'class="active"':'' !!}>
            <a href="{{ route('admin.menu-section') }}">
                <i class="menu-icon fa fa-pencil-square-o"></i>
                <span class="menu-text"> Menu manager </span>
            </a>
            <b class="arrow"></b>
        </li>

        <li {!! request()->is('admin/currency*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-money"></i>
                <span class="menu-text"> Currency Manager </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li {!! request()->is('admin/currency')?'class="active"':'' !!}>
                    <a href="{{ route('admin.currency') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List Currency
                    </a>

                    <b class="arrow"></b>
                </li>

                <li {!! request()->is('admin/currency/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.currency.create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add Currency
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/attribute-group*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Attribute Group </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>

            <ul class="submenu">
                <li {!! request()->is('admin/attribute-group')?'class="active"':'' !!}>
                    <a href="{{ route('admin.attribute-group') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List Attribute Group
                    </a>

                    <b class="arrow"></b>
                </li>

                <li {!! request()->is('admin/attribute-group/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.attribute-group.create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add Attribute Group
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li {!! request()->is('admin/attribute*')&& !request()->is('admin/attribute-group*')?'class="active open"':'' !!}>
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Attribute Manager </span>

                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>

            <ul class="submenu">
                <li {!! request()->is('admin/attribute')?'class="active"':'' !!}>
                    <a href="{{ route('admin.attribute') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        List Attribute
                    </a>

                    <b class="arrow"></b>
                </li>

                <li {!! request()->is('admin/attribute/create')?'class="active"':'' !!}>
                    <a href="{{ route('admin.attribute.create') }}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add Attribute
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>




    </ul><!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>
