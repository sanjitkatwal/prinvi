<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('title')?'has-error':'' }} " for="title">Title</label>

    <div class="col-sm-9">
        {!! Form::text('title', null, [
        'class'     => 'form-control',
        'placeholder' => 'Enter Title',
        ]) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('symbol')?'has-error':'' }} " for="title">Symbol</label>

    <div class="col-sm-9">
        {!! Form::text('symbol', null, [
        'class'     => 'form-control',
        'placeholder' => 'Enter Currency Symbol',
        ]) !!}
        @if ($errors->has('symbol'))
            <span class="help-block">
                <strong>{{ $errors->first('symbol') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('rate')?'has-error':'' }} " for="title">Rate</label>

    <div class="col-sm-9">
        {!! Form::number('rate', null, [
        'class'       => 'col-xs-10 col-sm-5',
        'placeholder' => 'Enter Currency Rate',
        'min'         => 0,
        'step'        => '.01',
        ]) !!}
        @if ($errors->has('rate'))
            <span class="help-block">
                <strong>{{ $errors->first('rate') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('status')?'has-error':'' }} " for="status">Status</label>

    <div class="col-sm-9">
        {!!
            Form::select('status', ['1' => 'Active', '0' => 'In-Active'])
        !!}
    </div>
</div>
<div class="space-4"></div>

<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">

        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            {{ $button }}
        </button>

        &nbsp; &nbsp;&nbsp;
        {!!
         Form::button('<i class="ace-icon fa fa-undo bigger-110">&nbsp; Reset</i>', [
            'class' =>'btn',
            'type'  => 'reset'
         ])
         !!}
    </div>
</div>
<div class="space-4"></div>
