@extends('admin.layouts.app')
@section('page_titel')
    {{ $_panel }}-List
@endsection



@section('content')

    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                @include('admin.includes.dashboard_link')

                <li>
                    <a href="{{ route($_base_route) }}">{{ $_panel }}</a>
                </li>
                <li class="active">List</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">

            <div class="page-header">
                <h1>
                    {{ $_panel }} Manager
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        List
                    </small>
                </h1>
                <button type="button" id="reorder-btn" class="btn btn-sm btn-primary pull-right" style="margin-top: -29px;">Order Currency</button>
            </div><!-- /.page-header -->

            <div class="row">
                @include('admin.includes.message');

                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <div class="col-xs-12">
                            {!! Form::open(['route' => $_base_route.'.reorder-currency', 'id' =>'reorder-form']) !!}
                            <table id="simple-table" class="table  table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center">
                                        <label class="pos-rel">SN
                                            <input type="checkbox" class="ace">
                                            <span class="lbl"></span>
                                        </label>
                                    </th>
                                    <th class="detail-col">Title</th>
                                    <th class="detail-col">Is Default</th>
                                    <th class="detail-col">Symbol</th>
                                    <th class="detail-col">Rate</th>
                                    <th>Status</th>
                                    <th class="hidden-480">Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($data['rows'] as $row)
                                    <tr>
                                        <td class="center">
                                            <label class="pos-rel">
                                                {{ $no++ }}
                                                <input type="checkbox" class="ace">
                                                <span class="lbl"></span>
                                            </label>
                                            <input type="hidden" name="id[]" value="{{$row->id}}">
                                        </td>

                                        <td>
                                          {!! $row->title !!}
                                        </td>
                                        <td>
                                            {!! $row->is_default !!}
                                        </td>
                                        <td>
                                            {!! $row->symbol !!}
                                        </td>
                                        <td>{!! $row->rate !!}</td>
                                        <td>
                                            @if($row->status == 1)
                                                <button class="btn btn-xs btn-success">Active</button>
                                            @else
                                                <button class="btn btn-xs btn-warning">In-active</button>
                                            @endif
                                        </td>
                                        <td>{!! isset($row->created_at)?$row->created_at->diffForHumans():'' !!}</td>

                                        <td>
                                            <div class="hidden-sm hidden-xs btn-group">
                                                <a href="{{ route($_base_route.'.edit',$row->id) }}" class="btn btn-xs btn-info">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                </a>

                                                @if($row->isDeleteble())
                                                    <a href="{{ route($_base_route.'.delete',$row->id) }}" class="btn btn-xs btn-danger confirm">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </a>
                                                    @endif

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                   <td colspan="8">{!! $data['rows']->links() !!}</td>
                                </tr>
                                </tbody>
                            </table>

                            {!! Form::close() !!}

                        </div><!-- /.span -->
                    </div><!-- /.row -->

                    <div class="hr hr-18 dotted hr-double"></div>

                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

    @endsection
@section('page_specific_script')
    @include('admin.layouts.common.jquery_sortable_script')

    <script>
        $(document).ready(function () {
            $('#reorder-btn').click(function () {
                $('#reorder-form').submit();
            })
        });
    </script>
@endsection
