@extends('admin.layouts.app')
@section('page_titel')
    {{ $_panel }}-List
@endsection



@section('content')

    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                @include('admin.includes.dashboard_link')

                <li>
                    <a href="{{ route($_base_route) }}">{{ $_panel }}</a>
                </li>
                <li class="active">List</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">

            <div class="page-header">
                <h1>
                    {{ $_panel }} Manager
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        List
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                @include('admin.includes.message');

                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="simple-table" class="table  table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="center">
                                        <label class="pos-rel">SN
                                            <input type="checkbox" class="ace">
                                            <span class="lbl"></span>
                                        </label>
                                    </th>
                                    <th>image</th>
                                    <th class="detail-col">Caption</th>
                                    <th>Status</th>
                                    <th>Rank</th>
                                    <th class="hidden-480">Created At</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($data['rows'] as $row)
                                    <tr>
                                        <td class="center">

                                            <label class="pos-rel">
                                                {{ $no++ }}
                                                <input type="checkbox" class="ace">
                                                <span class="lbl"></span>
                                            </label>
                                        </td>
                                        <td>
                                            @if($row->image)
                                                <img src="{{ asset('images/'.$_folder.'/'.$row->image) }}" style="width: 80px; height: 60px;">
                                                @else
                                            <p>No Image</p>
                                                @endif
                                        </td>
                                        <td>{!! $row->caption !!}</td>
                                        <td>{!! $row->status !!}</td>
                                        <td>{!! $row->rank !!}</td>
                                        <td>{!! isset($row->created_at)?$row->created_at->diffForHumans():'' !!}</td>

                                        <td>
                                            <div class="hidden-sm hidden-xs btn-group">
                                                <a href="{{ route($_base_route.'.show',$row->id) }}" class="btn btn-xs btn-success">
                                                    <i class="ace-icon fa fa-eye bigger-120"></i>
                                                </a>

                                                <a href="{{ route($_base_route.'.edit',$row->id) }}" class="btn btn-xs btn-info">
                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                </a>

                                                <a href="{{ route($_base_route.'.delete',$row->id) }}" class="btn btn-xs btn-danger">
                                                    <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                </a>

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                   <td colspan="7">{!! $data['rows']->links() !!}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!-- /.span -->
                    </div><!-- /.row -->

                    <div class="hr hr-18 dotted hr-double"></div>

                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

    @endsection
