@if(isset($data['row']))
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="main_image">Main Image</label>

        <div class="col-sm-9">
            @if($data['row']->image)
                <img src="{{ asset('images/'.$_folder.'/'.$data['row']->image) }}" style="width: 80px; height: 60px;">
            @else
                <p>No Image</p>
            @endif
        </div>
    </div>
    <div class="space-4"></div>

@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('main_image')?'has-error':'' }}" for="banner_image">Main Image</label>

    <div class="col-sm-9">
        {!! Form::file('main_image', ['class'=>'col-xs-10 col-sm-5']) !!}
        {{--<input type="file" name="banner_image" class="col-xs-10 col-sm-5">--}}
        @if ($errors->has('main_image'))
            <span class="help-block">
                <strong>{{ $errors->first('main_image') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    {!! Form::label('alt_text', 'Alternative Text', [
    'class' => 'col-sm-3 control-label no-padding-right',
    ])
     !!}
    <div class="col-sm-9">
        {!! Form::text('alt_text', null, [
            'class'         => 'col-xs-10 col-sm-5',
            'placeholder'   => 'Alternative Text',
        ]) !!}
        @if ($errors->has('alt_text'))
            <span class="help-block">
                <strong>{{ $errors->first('alt_text') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    {!! Form::label('caption', 'Caption', [
    'class' => 'col-sm-3 control-label no-padding-right',
    ])
     !!}
    <div class="col-sm-9">
        {!! Form::text('caption', null, [
            'class'         => 'col-xs-12 col-sm-12',
            'placeholder'   => 'Caption Text',
        ]) !!}
        @if ($errors->has('caption'))
            <span class="help-block">
                <strong>{{ $errors->first('caption') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>



<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('link')?'has-error':'' }} " for="link">Link</label>

    <div class="col-sm-9">
        {!! Form::textarea('link', null, [
        'class'     => 'form-control',
        'placeholder' => 'Description',
        'cols'      => 60,
        'rows'      => 7,
        ]) !!}
        @if ($errors->has('link'))
            <span class="help-block">
                <strong>{{ $errors->first('link') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    {!! Form::label('rank', 'Rank', [
    'class' => 'col-sm-3 control-label no-padding-right',
    ])
     !!}
    <div class="col-sm-9">
        {!! Form::text('rank', null, [
            'class'         => 'col-xs-12 col-sm-12',
            'placeholder'   => '1-n',
        ]) !!}
        @if ($errors->has('rank'))
            <span class="help-block">
                <strong>{{ $errors->first('rank') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('status')?'has-error':'' }} " for="status">Status</label>

    <div class="col-sm-9">
        {!!
            Form::select('status', ['' => 'Select Status', '1' => 'Active', '0' => 'In-Active'])
        !!}

        @if ($errors->has('status'))
            <span class="help-block">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        {{--{!!
         Form::button('<i class="ace-icon fa fa-check bigger-110"> &nbsp;{{ $button }}</i>', [
            'class' =>'btn btn-info',
            'type'  => 'submit'
         ])
         !!}--}}

        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            {{ $button }}
        </button>

        &nbsp; &nbsp;&nbsp;
        {!!
         Form::button('<i class="ace-icon fa fa-undo bigger-110">&nbsp; Reset</i>', [
            'class' =>'btn',
            'type'  => 'reset'
         ])
         !!}
    </div>
</div>
<div class="space-4"></div>
