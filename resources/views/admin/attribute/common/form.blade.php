<div class="form-group">

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right {{ $errors->has('attribute_group_id')?'has-error':'' }} " for="title">Group Name</label>

        <div class="col-sm-9">

                {!!
                    Form::select('attribute_group_id', $data['attribute_groups'], null, [
                    'class'     => 'form-control',
                    'placeholder' => 'Select Attribute Title',
                     ])
                !!}
            @if ($errors->has('attribute_group_id'))
                <span class="help-block">
                <strong>{{ $errors->first('attribute_group_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="space-4"></div>

<div class="form-group">

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('title')?'has-error':'' }} " for="title">Title</label>

    <div class="col-sm-9">
        {!! Form::text('title', null, [
        'class'     => 'form-control',
        'placeholder' => 'Enter Title',
        ]) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('status')?'has-error':'' }} " for="status">Status</label>

    <div class="col-sm-9">
        {!!
            Form::select('status', ['' => 'Select Status', '1' => 'Active', '0' => 'In-Active'])
        !!}
        @if ($errors->has('status'))
            <span class="help-block">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            {{ $button }}
        </button>

        &nbsp; &nbsp;&nbsp;
        {!!
         Form::button('<i class="ace-icon fa fa-undo bigger-110">&nbsp; Reset</i>', [
            'class' =>'btn',
            'type'  => 'reset'
         ])
         !!}
    </div>
</div>
<div class="space-4"></div>
