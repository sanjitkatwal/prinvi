@extends('admin.layouts.app')
@section('page_titel')
    {{ $_panel }}-List
@endsection



@section('content')

    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                @include('admin.includes.dashboard_link')

                <li>
                    <a href="{{ route($_base_route) }}">{{ $_panel }}</a>
                </li>
                <li class="active">List</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">

            <div class="page-header">
                <h1>
                    {{ $_panel }} Manager
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        List
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                @include('admin.includes.message');

                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="simple-table" class="table  table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Attribute Group</th>
                                    <th>Title</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>

                                <tr id="filter-row-tr">
                                    <th>
                                        <input type="text" class="form-control" id="filter_id" value="{{ request()->get('attribute-id') }}">
                                    </th>
                                    <th><input type="text" class="form-control" id="filter_attribute_group" value="{{ request()->get('attribute-group-title') }}"></th>
                                    <th><input type="text" class="form-control" id="filter_attribute_title" value="{{ request()->get('attribute-title') }}"></th>
                                    <th><input type="text" class="form-control" id="filter_attribute_date" value="{{ request()->get('attribute-date') }}"></th>
                                    <th>
                                        <a class="btn btn-xs btn-info" href="javascript:void(0);" id="filter-btn">
                                            <i class="icon-search bigger-120"></i>Search
                                        </a>
                                        <a class="btn btn-xs btn-info" href="javascript:void(0);" id="filter-reset-btn">
                                            <i class="icon-search bigger-120"></i>Reset
                                        </a>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                    @if($data['rows']->count() >0)
                                        @foreach($data['rows'] as $row)
                                            <tr>
                                                <td>{!! $no++ !!}</td>
                                                <td>{!! $row->attribute_group_title !!}</td>
                                                <td>{!! $row->title !!}</td>
                                                <td>{!! $row->created_at !!}</td>
                                                <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="{{ route($_base_route.'.show',$row->id) }}" class="btn btn-xs btn-success">
                                                        <i class="ace-icon fa fa-eye bigger-120"></i>
                                                    </a>

                                                    <a href="{{ route($_base_route.'.edit',$row->id) }}" class="btn btn-xs btn-info">
                                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                    </a>

                                                    <a href="{{ route($_base_route.'.delete',$row->id) }}" class="btn btn-xs btn-danger confirm">
                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                    </a>

                                                </div>
                                            </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="5">{!! $data['rows']->links() !!}</td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td colspan="5">No data found!</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div><!-- /.span -->
                    </div><!-- /.row -->

                    <div class="hr hr-18 dotted hr-double"></div>

                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

    @endsection

@section('page_specific_script')
    <script>
        $(document).ready(function () {
           $('#filter-btn').click(function () {
               var url = '{{ route($_base_route) }}';
               var filter_id = $('#filter_id').val();
               var filter_attribute_group_title = $('#filter_attribute_group').val();
               var filter_attribute_title = $('#filter_attribute_title').val();
               var filter_attribute_date = $('#filter_attribute_date').val();

               var flag = false;

               if(filter_id !==''){
                   url = url + '?attribute-id=' + filter_id;
                   flag = true;
               }
               if(filter_attribute_group_title !==''){
                   if(flag){
                       url = url + '&attribute-group-title=' + filter_attribute_group_title;
                   }else{
                       url = url + '?attribute-group-title=' + filter_attribute_group_title;
                       flag = true;
                   }
               }
               if(filter_attribute_title !==''){
                   if(flag){
                       url = url + '&attribute-title=' + filter_attribute_title;
                   }else{
                       url = url + '?attribute-title=' + filter_attribute_title;
                       flag = true;
                   }
               }
               if(filter_attribute_date !==''){
                   if(flag){
                       url = url + '&attribute-date=' + filter_attribute_date;
                   }else{
                       url = url + '?attribute-date=' + filter_attribute_date;
                       flag = true;
                   }
               }

               location.href = url;
           });

           $('#filter-reset-btn').click(function () {
              $('#filter-row-tr').find('input').val('');
           });
        });


    </script>
@endsection
