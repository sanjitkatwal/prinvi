@extends('admin.layouts.app')
@section('page_titel')
    {{ $_panel }}:create
@endsection



@section('content')

    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
               @include('admin.includes.dashboard_link')

                <li>
                    <a href="{{ route($_base_route) }}">{{ $_panel }}</a>
                </li>
                <li class="active">Create </li>
            </ul><!-- /.breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
                    <span class="input-icon">
                        <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
                        <i class="ace-icon fa fa-search nav-search-icon"></i>
                    </span>
                </form>
            </div><!-- /.nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $_panel }} Manager
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Create Form
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">

                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <form class="form-horizontal" action="{{ route($_base_route.'.store') }}" method="post" enctype="multipart/form-data" role="form">
                        @csrf

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('parent_id')?'has-error':'' }}" for="name">Parent Category </label>

                            <div class="col-sm-9">
                                <select name="parent_id">
                                    <option value="0">-- Is Parent --</option>
                                    @if($data['parent_categories']->count() > 0)
                                        @foreach($data['parent_categories'] as $category)
                                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                                        @endforeach
                                        @endif
                                    <option value=""></option>
                                </select>
                                @if ($errors->has('parent_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('parent_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('title')?'has-error':'' }}" for="name"> Title</label>

                            <div class="col-sm-9">
                                <input type="text" placeholder="Title" value="" name="title" class="col-xs-10 col-sm-5">
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('banner_image')?'has-error':'' }}" for="banner_image">Banner Image</label>

                            <div class="col-sm-9">
                                <input type="file" name="banner_image" class="col-xs-10 col-sm-5">
                                @if ($errors->has('banner_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('banner_image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('description')?'has-error':'' }} " for="description">Description</label>

                            <div class="col-sm-9">
                                <textarea name="description" id="" cols="60" rows="7" class="form-control"></textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('status')?'has-error':'' }} " for="status">Status</label>

                            <div class="col-sm-9">
                                <select name="status">
                                    <option>Select Status</option>
                                    <option value="0">In-active</option>
                                    <option value="1">Active</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Submit
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="ace-icon fa fa-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div>
                        </div>

                    </form>

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
@endsection

