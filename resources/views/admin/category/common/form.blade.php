<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('parent_id')?'has-error':'' }}" for="name">Parent Category </label>

    <div class="col-sm-9">
        {{--<select name="parent_id">
            <option value="0">-- Is Parent --</option>
            @if($data['parent_categories']->count() > 0)
                @foreach($data['parent_categories'] as $category)
                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                @endforeach
                @endif
            <option value=""></option>
        </select>--}}

        {!!
            Form::select('parent_id', $data['parent_categories'], null, [
            'class'     => 'col-xs-10 col-sm-5'
            ])
        !!}
        @if ($errors->has('parent_id'))
            <span class="help-block">
                <strong>{{ $errors->first('parent_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group">
    {!! Form::label('title', 'Title', [
    'class' => 'col-sm-3 control-label no-padding-right',
    ])
     !!}
    {{--
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('title')?'has-error':'' }}" for="name"> Title</label>
    --}}

    <div class="col-sm-9">
        {!! Form::text('title', null, [
            'class'         => 'col-xs-10 col-sm-5',
            'placeholder'   => 'Title',
        ]) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

@if(isset($data['row']))
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="banner_image">Existing Image</label>

        <div class="col-sm-9">
            @if($data['row']->image)
                <img src="{{ asset('images/'.$_folder.'/'.$data['row']->image) }}" style="width: 80px; height: 60px;">
                <label for="checkbok">
                    <input type="checkbox" name="remove_image">
                    <span>Remove Image</span>
                </label>
            @else
                <p>No Image</p>
            @endif
        </div>
    </div>
    <div class="space-4"></div>

@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('banner_image')?'has-error':'' }}" for="banner_image">Banner Image</label>

    <div class="col-sm-9">
        {!! Form::file('banner_image', ['class'=>'col-xs-10 col-sm-5']) !!}
        {{--<input type="file" name="banner_image" class="col-xs-10 col-sm-5">--}}
        @if ($errors->has('banner_image'))
            <span class="help-block">
                <strong>{{ $errors->first('banner_image') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('description')?'has-error':'' }} " for="description">Description</label>

    <div class="col-sm-9">
        {!! Form::textarea('description', null, [
        'class'     => 'form-control',
        'placeholder' => 'Description',
        'cols'      => 60,
        'rows'      => 7,
        ]) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('status')?'has-error':'' }} " for="status">Status</label>

    <div class="col-sm-9">
        {!!
            Form::select('status', ['' => 'Select Status', '1' => 'Active', '0' => 'In-Active'])
        !!}
    </div>
</div>
<div class="space-4"></div>

<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        {{--{!!
         Form::button('<i class="ace-icon fa fa-check bigger-110"> &nbsp;{{ $button }}</i>', [
            'class' =>'btn btn-info',
            'type'  => 'submit'
         ])
         !!}--}}

        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            {{ $button }}
        </button>

        &nbsp; &nbsp;&nbsp;
        {!!
         Form::button('<i class="ace-icon fa fa-undo bigger-110">&nbsp; Reset</i>', [
            'class' =>'btn',
            'type'  => 'reset'
         ])
         !!}
    </div>
</div>
<div class="space-4"></div>
