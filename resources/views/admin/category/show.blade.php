@extends('admin.layouts.app')
@section('page_titel')
    Profile:Show
@endsection

@section('content')

    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                @include('admin.includes.dashboard_link')

                <li>
                    <a href="{{ route($_base_route) }}">{{ $_panel }}</a>
                </li>
                <li class="active">Show</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">

            <div class="page-header">
                <h1>
                    {{ $_panel }} Manager
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Show
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="simple-table" class="table  table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 20%;">Column Name</th>
                                    <th>Value</th>
                                </tr>
                                </thead>
                                <tr>
                                    <td>Id</td>
                                    <td>{!! $data['row']->id !!}</td>
                                </tr>
                                <tr>
                                    <td>Title</td>
                                    <td>{!! $data['row']->title !!}</td>
                                </tr>
                                <tr>
                                    <td>Parent</td>
                                    <td>
                                        @if($data['row']->parent_id == 0)
                                            <p>Parent Category</p>

                                            @else
                                            <p>{{ \App\Models\Category::find($data['row']->parent_id)->title }}</p>
                                        @endif

                                    </td>
                                </tr>
                                <tr>
                                    <td>Slug</td>
                                    <td>{!! $data['row']->slug !!}</td>
                                </tr>
                                <tr>
                                    <td>Image</td>
                                    <td>
                                        @if($data['row']->image)
                                            <img src="{{ asset('images/'.$_folder.'/'.$data['row']->image) }}" style="width: 80px; height: 60px;">
                                        @else
                                            <p>No Image</p>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Description</td>
                                    <td>{!! $data['row']->description !!}</td>
                                </tr>

                                <tr>
                                    <td>Status</td>
                                    <td>
                                        @if($data['row']->status == 1)
                                            <button class="btn btn-xs btn-success">Active</button>
                                            @else
                                            <button class="btn btn-xs btn-warning">In-active</button>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Created At</td>
                                    <td>{!! $data['row']->created_at !!}</td>
                                </tr>

                                <tr>
                                    <td>Updated At</td>
                                    <td>{!! isset($data['row']->updated_at)?$data['row']->updated_at->diffForHumans():'' !!}</td>
                                </tr>


                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- /.span -->
                    </div><!-- /.row -->

                    <div class="hr hr-18 dotted hr-double"></div>

                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

    @endsection
