@extends('admin.layouts.app')
@section('page_titel')
    Profile
@endsection



@section('content')

    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
               @include('admin.includes.dashboard_link')

                <li>
                    <a href="#">Profile</a>
                </li>
                <li class="active">Update </li>
            </ul><!-- /.breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
                    <span class="input-icon">
                        <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
                        <i class="ace-icon fa fa-search nav-search-icon"></i>
                    </span>
                </form>
            </div><!-- /.nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    Profile Manager
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Update Form
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                @if(session()->has('success_message'))
                    <div class="alert alert-block alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>
                        <p>
                            <strong>
                                <i class="ace-icon fa fa-check"></i>
                                Well done!
                            </strong>
                            {{ session()->get('success_message') }}
                        </p>
                    </div>
                @endif
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <form class="form-horizontal" action="{{ route('admin.profile.update') }}" method="post" role="form">
                        @csrf

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('name')?'has-error':'' }}" for="name"> User Name</label>

                            <div class="col-sm-9">
                                <input type="text" placeholder="Username" value="{{ $data['user']->name }}" name="name" class="col-xs-10 col-sm-5">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('email')?'has-error':'' }} " for="email">Email</label>

                            <div class="col-sm-9">
                                <input type="email" placeholder="Valid Email" value="{{ $data['user']->email }}" name="email" class="col-xs-10 col-sm-5">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('first_name')?'has-error':'' }} " for="first_name">First Name</label>

                            <div class="col-sm-9">
                                <input type="text" name="first_name" placeholder="First Name" value="{{ $data['user']->first_name }}">
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('middle_name')?'has-error':'' }} " for="middle_name">Middle Name</label>

                            <div class="col-sm-9">
                                <input type="text" placeholder="Middle Name" value="{{ $data['user']->middle_name }}" name="middle_name" class="col-xs-10 col-sm-5">
                                @if ($errors->has('middle_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('middle_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('last_name')?'has-error':'' }} " for="last_name">Last Name</label>

                            <div class="col-sm-9">
                                <input type="text" placeholder="Last Name" value="{{ $data['user']->last_name }}" name="last_name" class="col-xs-10 col-sm-5">
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('contact')?'has-error':'' }} " for="contact">Phone</label>

                            <div class="col-sm-9">
                                <input type="number" placeholder="Valid Phone Number" value="{{ $data['user']->contact }}" name="contact" class="col-xs-10 col-sm-5">
                                @if ($errors->has('contact'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contact') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('address')?'has-error':'' }} " for="address">Address</label>

                            <div class="col-sm-9">
                                <textarea name="address" id="" cols="60" rows="7" class="form-control">{{ $data['user']->address }}</textarea>
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('password')?'has-error':'' }} " for="password">Password</label>

                            <div class="col-sm-9">
                                <input type="password" placeholder="Password" name="password" class="col-xs-10 col-sm-5">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right {{ $errors->has('password_confirmation')?'has-error':'' }} " for="password_confirmation">Confirm Password</label>

                            <div class="col-sm-9">
                                <input type="password" placeholder="Conform Password" name="password_confirmation" class="col-xs-10 col-sm-5">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    Update
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="ace-icon fa fa-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div>
                        </div>

                    </form>

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
@endsection

