@extends('admin.layouts.app')
@section('page_titel')
    Profile:Show
@endsection

@section('content')

    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                @include('admin.includes.dashboard_link')

                <li>
                    <a href="{{ route('admin.user') }}">User</a>
                </li>
                <li class="active">Show</li>
            </ul><!-- /.breadcrumb -->
        </div>

        <div class="page-content">

            <div class="page-header">
                <h1>
                    User Manager
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Show
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="simple-table" class="table  table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 20%;">Column Name</th>
                                    <th>Value</th>
                                </tr>
                                </thead>
                                <tr>
                                    <td>Id</td>
                                    <td>{!! $data['row']->id !!}</td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td>{!! $data['row']->name !!}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{!! $data['row']->email !!}</td>
                                </tr>
                                <tr>
                                    <td>Full Name</td>
                                    <td>{!! $data['row']->first_name.' '.$data['row']->middle_name.' '.$data['row']->last_name !!}</td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>{!! $data['row']->address !!}</td>
                                </tr>
                                <tr>
                                    <td>Contact</td>
                                    <td>{!! $data['row']->contact !!}</td>
                                </tr>

                                <tr>
                                    <td>Created At</td>
                                    <td>{!! $data['row']->created_at !!}</td>
                                </tr>

                                <tr>
                                    <td>Updated At</td>
                                    <td>{!! isset($data['row']->updated_at)?$data['row']->updated_at->diffForHumans():'' !!}</td>
                                </tr>


                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- /.span -->
                    </div><!-- /.row -->

                    <div class="hr hr-18 dotted hr-double"></div>

                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

    @endsection
