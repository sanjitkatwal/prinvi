@extends('admin.layouts.app')
@section('page_titel')
    {{ $_panel }}:create
@endsection



@section('content')

    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">

                @include('admin.includes.dashboard_link')

                <li>
                    <a href="{{ route($_base_route) }}">{{ $_panel }}</a>
                </li>
                <li class="active">Edit </li>
            </ul><!-- /.breadcrumb -->
            
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $_panel }} Manager
                    <small>
                        <i class="ace-icon fa fa-angle-double-right"></i>
                        Edit Form
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">

                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    {!! Form::model( $data['row'],[
                    'url'       => route($_base_route.'.update',$data['row']->id),
                    'class'     => 'form-horizontal',
                    'enctype'   => 'multipart/form-data'
                    ]) !!}

                    {!! Form::hidden('id', $data['row']->id) !!}

                    @include($_view_path.'.common.form', ['button' => 'Update'])


                    {!! Form::close() !!}

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
@endsection

@section('page_specific_script')

    <script>
        $(document).ready(function () {
            $('#page-html-loader-btn').click(function () {
                $.ajax({
                    method: 'POST',
                    url:    '{{ route('admin.menu-section.load-page-row') }}',
                    data: {
                      _token: '{{ csrf_token() }}',
                    },
                    success:function (response) {

                        var data = $.parseJSON(response);
                        $('#page_row_wrapper').append(data.html);
                    }
                });
            })
        })
    </script>

    @include('admin.layouts.common.jquery_sortable_script')

    @endsection

