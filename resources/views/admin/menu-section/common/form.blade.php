<div class="form-group">

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('title')?'has-error':'' }} " for="title">Title</label>

    <div class="col-sm-9">
        {!! Form::text('title', null, [
        'class'     => 'form-control',
        'placeholder' => 'Enter Title',
        ]) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('hint')?'has-error':'' }} " for="status">Hint</label>

    <div class="col-sm-9">
        {!!
            Form::textarea('hint',null, [
                'placeholder' => 'Hint',
                'class' => 'col-xs-10 col-sm-5',
            ] )
        !!}
        @if ($errors->has('hint'))
            <span class="help-block">
                <strong>{{ $errors->first('hint') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

@include($_view_path.'.common.page_section')

<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
       <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            {{ $button }}
        </button>

        &nbsp; &nbsp;&nbsp;
        {!!
         Form::button('<i class="ace-icon fa fa-undo bigger-110">&nbsp; Reset</i>', [
            'class' =>'btn',
            'type'  => 'reset'
         ])
         !!}
    </div>
</div>
<div class="space-4"></div>
