<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('page_type')?'has-error':'' }} " for="page_type">Choose Page Type</label>

    <div class="col-sm-9">
        {!!
            Form::select('page_type', [
                'content_page' => 'Content Page',
                'link_page' => 'Link Page'
            ], null, ['class' => 'col-sm-5', 'id' => 'page_type'])
        !!}
    </div>
    @if ($errors->has('page_type'))
        <span class="help-block">
                <strong>{{ $errors->first('page_type') }}</strong>
            </span>
    @endif
</div>
<div class="space-4"></div>


<div class="form-group">
    {!! Form::label('title', 'Title', [
    'class' => 'col-sm-3 control-label no-padding-right',
    ])
     !!}
    <div class="col-sm-9">
        {!! Form::text('title', null, [
            'class'         => 'col-xs-12 col-sm-12',
            'placeholder'   => 'Title',
        ]) !!}
        @if ($errors->has('title'))
            <span class="help-block">
                <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

@if(isset($data['row']))
    <div class="form-group content_type_section" style="display:none;">
        <label class="col-sm-3 control-label no-padding-right" for="main_image">Main Image</label>

        <div class="col-sm-9">
            @if($data['row']->image)
                <img src="{{ asset('images/'.$_folder.'/'.$data['row']->image) }}" style="width: 80px; height: 60px;">
            @else
                <p>No Image</p>
            @endif
        </div>
    </div>
    <div class="space-4"></div>

@endif

<div class="form-group content_type_section" style="display:none;">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('main_image')?'has-error':'' }}" for="banner_image">Main Image</label>

    <div class="col-sm-9">
        {!! Form::file('main_image', ['class'=>'col-xs-10 col-sm-5']) !!}
        {{--<input type="file" name="banner_image" class="col-xs-10 col-sm-5">--}}
        @if ($errors->has('main_image'))
            <span class="help-block">
                <strong>{{ $errors->first('main_image') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group content_type_section" style="display:none;">
    {!! Form::label('description', 'Description', [
    'class' => 'col-sm-3 control-label no-padding-right',
    ])
     !!}
    <div class="col-sm-9">
        {!! Form::textarea('description', null, [
            'class'     => 'form-control',
            'placeholder' => 'Description',
            'cols'      => 60,
            'rows'      => 7,
        ]) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group link_type_section" style="display:none;">
    {!! Form::label('link', 'Link', [
    'class' => 'col-sm-3 control-label no-padding-right',
    ])
     !!}
    <div class="col-sm-9">
        {!! Form::text('link', null, [
            'class'         => 'col-xs-12 col-sm-12',
            'placeholder'   => 'Link',
        ]) !!}
        @if ($errors->has('link'))
            <span class="help-block">
                <strong>{{ $errors->first('link') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right {{ $errors->has('status')?'has-error':'' }} " for="status">Status</label>

    <div class="col-sm-9">
        {!!
            Form::select('status', ['' => 'Select Status', '1' => 'Active', '0' => 'In-Active'])
        !!}

        @if ($errors->has('status'))
            <span class="help-block">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">

        <button class="btn btn-info" type="submit">
            <i class="icon-ok bigger-110"></i>
            {{ $button }}
        </button>

        &nbsp; &nbsp;&nbsp;
        {!!
         Form::button('<i class="ace-icon fa fa-undo bigger-110">&nbsp; Reset</i>', [
            'class' =>'btn',
            'type'  => 'reset'
         ])
         !!}
    </div>
</div>
<div class="space-4"></div>
