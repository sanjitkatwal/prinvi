<script type="text/javascript">

    init_page_type();

    $(document).ready(function () {
        $('#page_type').change(function () {
            var type = $(this).val();
            if(type == 'content_page'){
                $('.content_type_section').show();
                $('.link_type_section').hide();
            }else{
                $('.content_type_section').hide();
                $('.link_type_section').show();
            }
        })
    })

    function init_page_type() {
        var type = $('#page_type').val();
        if(type == 'content_page'){
            $('.content_type_section').show();
            $('.link_type_section').hide();
        }else{
            $('.content_type_section').hide();
            $('.link_type_section').show();
        }
    }
</script>
