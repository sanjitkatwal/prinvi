<?php

namespace App\Models;

class Menu extends BaseModel
{
    protected $table = 'menu_section';

    protected $fillable = ['title','created_at','hint', 'slug'];


    /**
     * The pages that belong to the Menu.
     */
    public function pages()
    {
        return $this->belongsToMany(Page::class);
    }
}
