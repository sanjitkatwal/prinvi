<?php

namespace App\Models;

class Brand extends BaseModel
{
    protected $table = 'brands';

    protected $fillable = ['title','status', 'slug'];
}
