<?php

namespace App\Models;

class Page extends BaseModel
{
    protected $table = 'pages';
    protected $fillable = [
        'page_type','title', 'slug', 'image', 'link', 'description', 'status'
    ];


    /**
     * The Menus that belong to the Pages.
     */
    public function menuSection()
    {
        return $this->belongsToMany(Menu::class);
    }
}
