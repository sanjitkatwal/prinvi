<?php

namespace App\Models;


class Tag extends BaseModel
{
    protected $table = 'tags';

    protected $fillable = ['title','status', 'slug'];
}
