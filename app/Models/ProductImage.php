<?php

namespace App\Models;

class ProductImage extends BaseModel
{
    protected $table ='products_images';
    protected $fillable = [
        'product_id','image','alt_text', 'caption', 'rank','status',''
    ];

}
