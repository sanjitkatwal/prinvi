<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'image', 'alt_text', 'caption', 'link', 'rank', 'status'
    ];
}
