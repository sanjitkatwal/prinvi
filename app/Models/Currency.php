<?php

namespace App\Models;


class Currency extends BaseModel
{
    protected $fillable = [
        'is_default', 'title', 'slug', 'symbol', 'rate', 'status', 'rank'
    ];

    public function isDeleteble()
    {
        if($this->slug =='usd')
            return false;

        return true;
    }
}
