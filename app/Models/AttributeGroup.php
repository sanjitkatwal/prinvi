<?php

namespace App\Models;


class AttributeGroup extends BaseModel
{
    protected $table = 'attribute_groups';

    protected $fillable = ['title','slug', 'status'];


    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }
}
