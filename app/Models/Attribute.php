<?php

namespace App\Models;


class Attribute extends BaseModel
{
    protected $table = 'attributes';

    protected $fillable = ['attribute_group_id','title','slug', 'status'];

    public function attributeGroup()
    {
        return $this->belongsTo(AttributeGroup::class);
    }
}
