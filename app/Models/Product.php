<?php

namespace App\Models;

class Product extends BaseModel
{
    protected $fillable = [
      'title', 'slug', 'main_image', 'old_price', 'new_price', 'quantity',
        'short_desc','long_desc', 'hits','category_id','brand_id', 'status',
        'seo_description','seo_keywords','seo_title'
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function brand(){
        return $this->belongsTo(Brand::class);
    }
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'products_tags', 'product_id', 'tag_id');
    }
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'products_attributes', 'product_id', 'attribute_id');
    }
}
