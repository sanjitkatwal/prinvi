<?php

namespace App\Models;


class Category extends BaseModel
{
    protected $fillable = [
      'id', 'title', 'parent_id', 'slug', 'description', 'image', 'status'
    ];
}
