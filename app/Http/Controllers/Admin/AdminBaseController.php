<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View, File, Image;

class AdminBaseController extends Controller
{
    public $model;
    public $file_name;


    protected function loadCommonDataToView($view_path)
    {
        View::composer($view_path, function ($view){
            $view->with('dashboard_url', route('admin.dashboard'));
            $view->with('_panel', $this->panel);
            $view->with('_base_route', $this->base_route);
            $view->with('_view_path', $this->view_path);
            $view->with('_folder', property_exists($this, 'folder')?$this->folder:'');
        });

        return $view_path;

    }

    public function checkFolderExist()
    {
        if(!file_exists(public_path($this->folder_path))){
            mkdir(public_path($this->folder_path));
        }
    }

    public function getRandormImageName()
    {
        return rand(2000,9999);
    }

    public function rowExist($row)
    {
        if(!$row){
            request()->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route)->send();
        }
    }

    protected function processImage($file, $dimension_conf = null)
    {
        $this->file_name = $this->getRandormImageName().'_'.$file->getClientOriginalName();

        //folder exist
        $this->checkFolderExist();
        $file->move(public_path($this->folder_path), $this->file_name);

        // upload thumb image
        if($dimension_conf){
            $image_thumb_config = $dimension_conf;
            foreach ($image_thumb_config as $thumb_config) {
                $thumb_image = Image::make(($this->folder_path).DIRECTORY_SEPARATOR.$this->file_name);
                $thumb_image->resize($thumb_config['width'], $thumb_config['height']);
                $thumb_image->save(public_path($this->folder_path).DIRECTORY_SEPARATOR.$thumb_config['width'].'_'.$thumb_config['height'].'_'.$this->file_name);
            }
        }
    }

    protected function removeFile($path)
    {
        if (File::exists(public_path($path)))
            File::delete(public_path($path));
    }
}
