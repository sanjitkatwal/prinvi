<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Menu\MenuEditValidation;
use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Page;
use File, Str;

class MenuSectionController extends AdminBaseController
{
    protected $base_route= 'admin.menu-section';
    protected $view_path = 'admin.menu-section';
    protected $panel = 'Menu Section';


    public function __construct(Menu $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id','title', 'slug', 'hint', 'created_at')
            ->orderBy('id', 'asc')
            ->paginate(10);

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'))->with('no', 1);
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);
        $data['pages'] = Page::select('id', 'title')
                ->where('status', 1)->get();

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function loadPageRow()
    {
        $response = [];
        $data = [];
        $data['pages'] = Page::select('id', 'title')
            ->where('status', 1)->get();
        $response['html'] = view($this->view_path.'.common.page_content_row', compact('data'))->render();
        return response()->json(json_encode($response));
    }


    public function update(MenuEditValidation $request, $id)
    {

        $row = $this->model->find($id);

        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);
        //dd($request->all());


        $row->update($request->all());
        //dd($request->all());
        //$row->pages()->sync($request->get('pages'));
        /*$row->pages()->sync([
            4 => [
                'rank'  => 1
            ],
            3 => [
                'rank'  => 2
            ],
            3 => [
                'rank'  => 3
            ],
        ]);*/


            $pivot_data = [];
            foreach ($request->get('pages') as $key => $page_id){
                $pivot_data[$page_id] = [
                    'rank'  =>$key + 1,
                ];
            }
            $row->pages()->sync($pivot_data);

        $request->session()->flash('success_message', $this->panel . ' Updated Successfully');
        return redirect()->route($this->base_route);

    }


}
