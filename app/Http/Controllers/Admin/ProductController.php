<?php

namespace App\Http\Controllers\Admin;

use File, Image;
use App\Models\Tag;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Currency;
use App\Models\Category;
use Illuminate\Support\Str;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use App\Models\AttributeGroup;
use Hamcrest\DiagnosingMatcher;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Product\editProductvalidation;
use App\Http\Requests\Product\addProductvalidation;

class ProductController extends AdminBaseController
{
    protected $base_route= 'admin.product';
    protected $view_path = 'admin.product';
    protected $panel = 'Product';
    protected $folder = 'product';
    protected $folder_path;

    public function __construct(Product $model)
    {
        $this->model = $model;
        $this->folder_path = 'images'.DIRECTORY_SEPARATOR.$this->folder;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id','created_at', 'title','main_image','status')
        ->orderBy('id', 'desc')
        ->paginate(5);


        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'))->with('no', 1);
    }

    public function create()
    {
       $data = [];
       $data['categories'] =  $this->getCategoryList();
       $data['tags']       =  Tag::active()->select('id', 'title')->get();
       $data['brands']     =  Brand::active()->select('id', 'title')->get();
       $data['attribute_groups'] = AttributeGroup::active()->orderBy('title')->get();
       $data['attributes'] = $data['attribute_groups']->first()->attributes()->orderBy('title')->get();

       return view(parent::loadCommonDataToView($this->view_path.'.create'), compact('data'));

   }

   public function loadGalleryRow()
   {
    $response = [];
    $data = [];
    $response['html'] = view($this->view_path.'.common.gallery_row', compact('data'))->render();
    return response()->json(json_encode($response));
}

public function loadAttributeByGroup(Request $request)
{
    $response = [];
    $att_group = AttributeGroup::active()->where('id', $request->get('attribute_group_id'))->first();
    $attributes = $att_group->attributes()->orderBy('title')->get();
    $response['html'] = view($this->view_path.'.common.attribute_options', compact('attributes'))->render();
    return response()->json(json_encode($response));

}

public function loadAttributeRow()
{
    $response = [];
    $data = [];
    $data['attribute_groups'] = AttributeGroup::active()->orderBy('title')->get();
    $data['attributes'] = $data['attribute_groups']->first()->attributes()->orderBy('title')->get();
    $response['html'] = view($this->view_path.'.common.attribute_row', compact('data'))->render();
    return response()->json(json_encode($response));
}

public function loadSlug(Request $request)
{
    $response = [];
    $response['error'] = true;
    if(Product::where('title', $request->get('title'))->count() > 0){
        $response['message'] = '<strong>'.$request->get('title').'</strong>'.'&nbsp; title already exist.';
    }else{
        $response['slug'] = Str::slug($request->get('title', '-'));
        $response['error'] = false;

    }
    return response()->json(json_encode($response));
}

public function store(addProductvalidation $request)
{
    $this->storeImage($request);

    $product = $this->model->create($request->all());
    $product->tags()->sync($request->get('tags'));

    $this->storeAttributes($request, $product);
    $this->storeGalleryImages($request, $product);

    $request->session()->flash('success_message', $this->panel. ' Added Successfully');
    return redirect()->route($this->base_route);
}

public function show($id)
{
    $data = [];
    $data['row'] = Product::find($id);
    $data['currency'] = Currency::where('is_default', 1)->first();
    parent::rowExist($data['row']);

    $data['attributes'] = $this->getProductAttributeInfo($id);

    $data['gallery_images'] = $data['row']->images()
    ->select('image', 'alt_text', 'caption', 'status')
    ->orderBy('rank')
    ->get();

    return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
}

public function edit(Request $request, $id)
{
    $data = [];
    $data['row'] = $this->model->find($id);
    parent::rowExist($data['row']);

    $data['categories'] =  $this->getCategoryList();
    $data['tags']       =  Tag::active()->select('id', 'title')->get();
    $data['brands']     =  Brand::active()->select('id', 'title')->get();
    $data['attribute_groups'] = AttributeGroup::active()->orderBy('title')->get();
        //$data['attributes'] = $data['attribute_groups']->first()->attributes()->orderBy('title')->get();
    $data['attributes'] = $this->getFormatedDataForAttribute($id);

    return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));
}

public function update(editProductvalidation $request, $id)
{
    $row = $this->model::find($id);
    $this->storeImage($request, $row);
    $row->update($request->all());

    $this->storeAttributes($request, $row);
    $this->storeGalleryImagesEdit($request, $row);

    $request->session()->flash('success_message', $this->panel . ' Updated Successfully');
    return redirect()->route($this->base_route);

}

public function destroy(Request $request, $id)
{
    $row = Category::find($id);

    if(!$row){
        $request->session()->flash('error_message', 'Invalid request');
        return redirect()->route($this->base_route);
    }

        //remove the image
    if(File::exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image))){
        File::delete(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image));
    }
    $row->delete();
    $request->session()->flash('error_message', $this->panel.'Data Deleted Successfully');
    return redirect()->route($this->base_route);
}

protected function getParentCategoryForSelect(){
    $data = [];
    $data['parent_categories'] = Category::where('parent_id', 0)->pluck('title', 'id')->toArray();
        //$data['parent_categories'] = array_merge([0 => '- Is Parent -'], $data['parent_categories']);

    return [0 => '- Is Parent -'] + $data['parent_categories'];
}

public function getCategoryList()
{
    $categories = Category::active()->select('id','title', 'parent_id')->get();
    foreach ($categories as $row){

        if($row->parent_id !==0){
            $parent = Category::find($row->parent_id);
            $row->title = $parent->title .'>>' . $row->title;
        }
    }

        //dd($categories);
    return $categories;
}

protected function storeImage(Request $request, $product = null)
{
    if($request->hasFile('image')){
        $image_thumb_config = config('cms.product.image-dimentions.main-image');
        $this->processImage($request->file('image'), $image_thumb_config);

        $request->request->add(['main_image' => $this->file_name]);

            // if this is edit or not
        if($product){
                //remove old image
            parent::removeFile($this->folder_path.DIRECTORY_SEPARATOR.$product->main_image);

        }
    }


}

protected function storeAttributes(Request $request, Product $product)
{
        //store the attributes
    $pivot_data = [];
    foreach ($request->get('attribute') as $key => $attribute_id){
        $pivot_data[$attribute_id] = [
            'content'  => $request->get('attribute_value')[$key],
        ];
    }
    $product->attributes()->sync($pivot_data);
}

protected function storeGalleryImages(Request $request, Product $product)
{
        //upload galleries
    if($request->hasFile('gallery_image')){
        foreach ($request->file('gallery_image') as $key => $g_image){

            $image_thumb_config = config('cms.product.image-dimentions.gallery-image');
            $this->processImage($g_image, $image_thumb_config);

            ProductImage::create([
                'product_id' => $product->id,
                'image'      => $this->file_name,
                'alt_text'   => $request->get('gallery_alt_text')[$key],
                'caption'    => $request->get('gallery_caption')[$key],
                'rank'       => $key + 1,
                'status'     => $request->get('gallery_status')[$key],
            ]);
        }
    }
}

protected function storeGalleryImagesEdit(Request $request, Product $product)
{
    $image_thumb_config = config('cms.product.image-dimentions.gallery-image');
        //upload galleries
    $product_image_ids = [];
    if($request->get('gallery_alt_text')){
        foreach ($request->get('gallery_alt_text') as $key => $alt_text){
            $product_image_id = $request->get('gallery_id')[$key];
            if($product_image_id){

                    //update row
                $product_image = ProductImage::find($product_image_id);

                    // old image vako thau ma new image upload garda
                $file = $request->hasFile('gallery_image')[$key];
                if($file){
                        //upload new image & thumbnails
                    parent::processImage($file, $image_thumb_config);

                        //remove old image and thumbnails from folder
                    parent::removeFile($this->folder_path.DIRECTORY_SEPARATOR.$product_image->image);
                    foreach ($image_thumb_config as $config){
                        $prefix = $config['width'].'_'.$config['height'].'_';
                        parent::removeFile($this->folder_path.DIRECTORY_SEPARATOR.$prefix.$product_image->image);
                    }
                }
                $product_image->update([
                    'alt_text'  =>$alt_text,
                    'image'     =>$this->file_name?$this->file_name:$product_image->image,
                    'caption'   =>$request->get('gallery_caption')[$key],
                    'rank'      =>$key + 1,
                    'status'    =>$request->get('gallery_status')[$key],
                ]);

                $product_image_ids[] = $product_image_id;

            }else{
                    //create the image (insert new image)
                $this->processImage($request->file('gallery_image')[$key], $image_thumb_config);

                $product_image = ProductImage::create([
                    'product_id' => $product->id,
                    'image'      => $this->file_name,
                    'alt_text'   => $alt_text,
                    'caption'    => $request->get('gallery_caption')[$key],
                    'rank'       => $key + 1,
                    'status'     => $request->get('gallery_status')[$key],
                ]);

                $product_image_ids[] = $product_image->id;
            }
        }

        $removing_rows = ProductImage::where('product_id', $product->id)
        ->whereNotIn('id', $product_image_ids)
        ->get();

            //remove image from folder
        foreach ($removing_rows as $removing_row){
            if($removing_row->image){
                parent::removeFile($this->folder_path.DIRECTORY_SEPARATOR.$removing_row->image);
                foreach ($image_thumb_config as $config){
                    $prefix = $config['width'].'_'.$config['height'].'_';
                    parent::removeFile($this->folder_path.DIRECTORY_SEPARATOR.$prefix.$removing_row->image);
                }
            }
        }

            //remove rows from table
        ProductImage::where('product_id', $product->id)
        ->whereNotIn('id', $product_image_ids)->delete();
            //$removing_rows->delete();
    }else{
        $removing_rows = ProductImage::where('product_id', $product->id)
        ->get();

            //remove image from folder
        foreach ($removing_rows as $removing_row){
            if($removing_row->image){
                parent::removeFile($this->folder_path.DIRECTORY_SEPARATOR.$removing_row->image);
                foreach ($image_thumb_config as $config){
                    $prefix = $config['width'].'_'.$config['height'].'_';
                    parent::removeFile($this->folder_path.DIRECTORY_SEPARATOR.$prefix.$removing_row->image);
                }
            }
        }
            //remove rows from table
        ProductImage::where('product_id', $product->id)
        ->delete();
    }

}

protected function getProductAttributeInfo($product_id)
{
    return DB::table('products_attributes as pa')
    ->select('pa.product_id','a.attribute_group_id', 'pa.attribute_id', 'pa.content', 'a.title as attribute_title',
        'a.attribute_group_id','ag.title as group_title')
    ->join('attributes as a', 'a.id', '=', 'pa.attribute_id' )
    ->join('attribute_groups as ag', 'ag.id', '=', 'a.attribute_group_id' )
    ->where('pa.product_id', $product_id)
    ->get();
}

protected function getFormatedDataForAttribute($product_id)
{
    $data = [];
    $data['attributes'] = $this->getProductAttributeInfo($product_id);

    foreach ($data['attributes'] as $attribute){
        $attribute->attribute_list = AttributeGroup::find($attribute->attribute_group_id)
        ->attributes()->get(['id','title']);
    }

    return $data['attributes'];
}

}
