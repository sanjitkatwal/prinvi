<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Slider\sliderAddValidation;
use App\Http\Requests\Slider\sliderEditValidation;
use App\Models\Slider;
use Illuminate\Http\Request;
use File;

class SliderController extends AdminBaseController
{
    protected $base_route= 'admin.slider';
    protected $view_path = 'admin.slider';
    protected $panel = 'Slider';
    protected $folder = 'slider';
    protected $folder_path;


    public function __construct(Slider $model)
    {
        $this->model = $model;
        $this->folder_path = 'images'.DIRECTORY_SEPARATOR.$this->folder;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id','image', 'caption', 'status', 'rank')
            ->orderBy('rank', 'asc')
            ->paginate(5);

        //dd($data['rows']);

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'))->with('no', 1);
    }

    public function create()
    {
        return view(parent::loadCommonDataToView($this->view_path.'.create'));

    }

    public function store(sliderAddValidation $request)
    {
        if($request->hasFile('main_image')){
            $file = $request->file('main_image');
            $file_name = parent::getRandormImageName().'_'.$file->getClientOriginalName();

            //folder exist
            $this->checkFolderExist();
            $file->move(public_path($this->folder_path), $file_name);

            $request->request->add(['image' => $file_name]);
        }
        $this->model->create($request->all());
        $request->session()->flash('success_message', $this->panel. ' Added Successfully');
        return redirect()->route($this->base_route);
    }

    public function show($id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function update(sliderEditValidation $request, $id)
    {
        //dd($request->all());
        $row = $this->model->find($id);

       if ($request->hasFile('main_image')) {
            $file = $request->file('main_image');
            $file_name = parent::getRandormImageName() . '_' . $file->getClientOriginalName();

            //folder exist
            $this->checkFolderExist();
            $file->move(public_path($this->folder_path), $file_name);
            $request->request->add(['image' => $file_name]);

            //Remove old image
            if (File::exists(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image))) {
                File::delete(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image));
            }
        }


        $row->update($request->all());

        $request->session()->flash('success_message', $this->panel . ' Updated Successfully');
        return redirect()->route($this->base_route);

    }

    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);

        if(!$row){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        //remove the image
        if(File::exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image))){
            File::delete(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image));
        }
        $row->delete();
        $request->session()->flash('error_message', $this->panel.'Data Deleted Successfully');
        return redirect()->route($this->base_route);
    }

}
