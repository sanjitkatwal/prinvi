<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\AddFormValidation;
use App\Http\Requests\Category\categoryAddValidation;
use App\Http\Requests\Category\categoryEditValidation;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use File;

class CategoryController extends AdminBaseController
{
    protected $base_route= 'admin.category';
    protected $view_path = 'admin.category';
    protected $panel = 'Category';
    protected $folder = 'category';
    protected $folder_path;
	protected $test_folder;
	

    public function __construct()
    {
        $this->folder_path = 'images'.DIRECTORY_SEPARATOR.$this->folder;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = Category::select('id','title', 'parent_id', 'slug', 'description', 'image','created_at')
            ->orderBy('id', 'desc')
            ->paginate(5);

        foreach ($data['rows'] as $row){
            if ($row->parent_id == 0){
                $row->parent = '';
            }else{
                $parent = Category::find($row->parent_id);
                $row->parent = $parent['title'];
            }
        }

        //dd($data['rows']);

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'))->with('no', 1);
    }

    public function create()
    {
       $data = [];
       $data['parent_categories'] =  $this->getParentCategoryForSelect();

        return view(parent::loadCommonDataToView($this->view_path.'.create'), compact('data'));

    }

    public function store(categoryAddValidation $request)
    {
        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        if($request->hasFile('banner_image')){
            $file = $request->file('banner_image');
            $file_name = parent::getRandormImageName().'_'.$file->getClientOriginalName();

            //folder exist
            $this->checkFolderExist();
            $file->move(public_path($this->folder_path), $file_name);

            $request->request->add(['image' => $file_name]);
        }
        Category::create($request->all());
        $request->session()->flash('success_message', $this->panel. ' Added Successfully');
        return redirect()->route($this->base_route);
    }

    public function show($id)
    {
        $data = [];
        $data['row'] = Category::find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = Category::find($id);

        if(!$data['row']){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        $data['parent_categories'] = $this->getParentCategoryForSelect();

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        $row = Category::find($id);
        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        //remove old image if checkbox is checked
        if($request->has('remove_image')) {
            if($row->image){
                if (File::exists(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image))) {
                    File::delete(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image));
                }
                $row->image = null;
                $row->save();
            }
        }
       if ($request->hasFile('banner_image')) {
            $file = $request->file('banner_image');
            $file_name = parent::getRandormImageName() . '_' . $file->getClientOriginalName();

            //folder exist
            $this->checkFolderExist();
            $file->move(public_path($this->folder_path), $file_name);
            $request->request->add(['image' => $file_name]);

            //Remove old image
            if (File::exists(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image))) {
                File::delete(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image));
            }
        }


        $row->update($request->all());

        $request->session()->flash('success_message', $this->panel . ' Updated Successfully');
        return redirect()->route($this->base_route);

    }

    public function destroy(Request $request, $id)
    {
        $row = Category::find($id);

        if(!$row){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        //remove the image
        if(File::exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image))){
            File::delete(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image));
        }
        $row->delete();
        $request->session()->flash('error_message', $this->panel.'Data Deleted Successfully');
        return redirect()->route($this->base_route);
    }

    protected function getParentCategoryForSelect(){
        $data = [];
        $data['parent_categories'] = Category::where('parent_id', 0)->pluck('title', 'id')->toArray();
        //$data['parent_categories'] = array_merge([0 => '- Is Parent -'], $data['parent_categories']);

        return [0 => '- Is Parent -'] + $data['parent_categories'];
    }
}
