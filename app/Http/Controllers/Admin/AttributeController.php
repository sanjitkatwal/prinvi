<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Attribute\AttributeAddValidation;
use App\Http\Requests\Attribute\AttributeEditValidation;
use App\Models\Attribute;
use App\Models\AttributeGroup;
use File, Str;
use Illuminate\Http\Request;

class AttributeController extends AdminBaseController
{
    protected $base_route= 'admin.attribute';
    protected $view_path = 'admin.attribute';
    protected $panel = 'Attribute';


    public function __construct(Attribute $model)
    {
        $this->model = $model;
    }

    /*public function index()
    {
        $data = [];

        $data['rows'] = $this->model->select('attributes.id', 'attributes.created_at', 'attributes.attribute_group_id',
            'attributes.title', 'attributes.status','attribute_groups.title as attribute_group_title')

            ->join('attribute_groups', 'attribute_groups.id', '=','attributes.attribute_group_id')
            ->orderBy('attributes.id', 'desc')
            ->paginate(5);

        //dd($data['rows']);

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'))->with('no', 1);
    }*/

    public function index(Request $request)
    {
        $data = [];

        $data['rows'] = $this->model->select('attributes.id', 'attributes.created_at', 'attributes.attribute_group_id',
            'attributes.title', 'attributes.status','attribute_groups.title as attribute_group_title')

        ->join('attribute_groups', 'attribute_groups.id', '=','attributes.attribute_group_id')
        ->orderBy('attributes.id', 'desc')
        ->where(function ($query) use ($request){

            if($request->has('attribute-id')){
                $query->where('attributes.id', 'like', $request->get('attribute-id'));
            }
            if($request->has('attribute-group-title')){
                $query->where('attribute_groups.title', 'like', '%'.$request->get('attribute-group-title').'%');
            }
            if($request->has('attribute-title')){
                $query->where('attributes.title', 'like', '%'.$request->get('attribute-title').'%');
            }

                /*if($request->has('attribute-date')){
                    $query->where('attributes.title', 'like', '%'.$request->get('attribute-date').'%');
                }*/
            })
        ->paginate(15);

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'))->with('no', 1);
    }

    public function create()
    {
        $data = [];
        $data['attribute_groups'] = AttributeGroup::pluck('title','id');//pluck le column lae array ma tanxa

        return view(parent::loadCommonDataToView($this->view_path.'.create'), compact('data'));

    }

    public function store(AttributeAddValidation $request)
    {
        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        $this->model->create($request->all());
        $request->session()->flash('success_message', $this->panel. ' Added Successfully');
        return redirect()->route($this->base_route);
    }

    public function show($id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);

        if(!$data['row']){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }
        $data['attribute_groups'] = AttributeGroup::pluck('title','id');
        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function update(AttributeEditValidation $request, $id)
    {
        //dd($request->all());
        $row = $this->model->find($id);

        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        $row->update($request->all());

        $request->session()->flash('success_message', $this->panel . ' Updated Successfully');
        return redirect()->route($this->base_route);

    }

    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);

        if(!$row){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        $row->delete();
        $request->session()->flash('error_message', $this->panel.'Data Deleted Successfully');
        return redirect()->route($this->base_route);
    }
    
}
