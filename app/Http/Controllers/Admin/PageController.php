<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\Page\PageAddValidation;
use App\Http\Requests\Page\PageEditValidation;
use App\Models\Page;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use File;

class PageController extends AdminBaseController
{
    protected $base_route= 'admin.page';
    protected $view_path = 'admin.page';
    protected $panel = 'Page';
    protected $folder = 'page';
    protected $folder_path;


    public function __construct(Page $model)
    {
        $this->model = $model;
        $this->folder_path = 'images'.DIRECTORY_SEPARATOR.$this->folder;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id','image', 'title', 'status', 'page_type', 'created_at')
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'))->with('no', 1);
    }

    public function create()
    {
        return view(parent::loadCommonDataToView($this->view_path.'.create'));

    }

    public function store(PageAddValidation $request)
    {
        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        if($request->hasFile('main_image')){
            $file = $request->file('main_image');
            $file_name = parent::getRandormImageName().'_'.$file->getClientOriginalName();

            //folder exist
            $this->checkFolderExist();
            $file->move(public_path($this->folder_path), $file_name);

            $request->request->add(['image' => $file_name]);
        }
        $this->model->create($request->all());
        $request->session()->flash('success_message', $this->panel. ' Added Successfully');
        return redirect()->route($this->base_route);
    }

    public function show($id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function update(PageEditValidation $request, $id)
    {
        //dd($request->all());
        $row = $this->model->find($id);

        if($request->get('page_type') !== $row->page_type) {
            if($row->page_type == 'content_page'){
                if ($row->image){
                    if(File::exists(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image))) {
                        File::delete(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image));
                    }
                    $row->image = null;
                    $row->description = null;
                    $row->save();

                }

            }elseif ($row->page_type == 'link_page'){
                $row->link = null;
                $row->save();
            }
        }

        $file_name = '';
       if ($request->hasFile('main_image')) {
            $file = $request->file('main_image');
            $file_name = parent::getRandormImageName() . '_' . $file->getClientOriginalName();

            //folder exist
            $this->checkFolderExist();
            $file->move(public_path($this->folder_path), $file_name);
            $request->request->add(['image' => $file_name]);

            //Remove old image
            if (File::exists(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image))) {
                File::delete(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image));
            }
        }

        $attributes = [
            'page_type'    =>$request->get('page_type'),
            'title'        =>$request->get('title'),
            'slug'         =>Str::slug($request->get('title')),
            'status'       =>$request->get('status'),
            ];
       if($request->get('page_type') == 'content_page'){
           $attributes = array_merge($attributes, [
               'image'        =>$file_name?$file_name:$row->image,
               'description'  =>$request->get('description'),
           ]);
       }else{
           $attributes = array_merge($attributes, [
               'link'         =>$request->get('link'),
           ]);
       }

        $row->update($attributes);

        $request->session()->flash('success_message', $this->panel . ' Updated Successfully');
        return redirect()->route($this->base_route);

    }

    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);
        parent::rowExist($row);
        /*if(!$row){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }*/

        //remove the image
        if(File::exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image))){
            File::delete(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image));
        }
        $row->delete();
        $request->session()->flash('error_message', $this->panel. 'Data Deleted Successfully');
        return redirect()->route($this->base_route);
    }

}
