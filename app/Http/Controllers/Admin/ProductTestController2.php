<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Product\addProductvalidation;
use App\Models\AttributeGroup;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Category;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Tag;
use File, Image;

class ProductTestController2 extends AdminBaseController
{
    protected $base_route= 'admin.product';
    protected $view_path = 'admin.product';
    protected $panel = 'Product';
    protected $folder = 'product';
    protected $folder_path;

    public function __construct(Product $model)
    {
        $this->model = $model;
        $this->folder_path = 'images'.DIRECTORY_SEPARATOR.$this->folder;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id','created_at', 'title','main_image','status')
            ->orderBy('id', 'desc')
            ->paginate(5);


        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'))->with('no', 1);
    }

    public function create()
    {
       $data = [];
       $data['categories'] =  $this->getCategoryList();
       $data['tags']       =  Tag::active()->select('id', 'title')->get();
       $data['brands']     =  Brand::active()->select('id', 'title')->get();
       $data['attribute_groups'] = AttributeGroup::active()->orderBy('title')->get();
       $data['attributes'] = $data['attribute_groups']->first()->attributes()->orderBy('title')->get();

       return view(parent::loadCommonDataToView($this->view_path.'.create'), compact('data'));

    }

    public function loadGalleryRow()
    {
        $response = [];
        $data = [];
        $response['html'] = view($this->view_path.'.common.gallery_row', compact('data'))->render();
        return response()->json(json_encode($response));
    }

    public function loadAttributeByGroup(Request $request)
    {
        $response = [];
        $att_group = AttributeGroup::active()->where('id', $request->get('attribute_group_id'))->first();
        $attributes = $att_group->attributes()->orderBy('title')->get();
        $response['html'] = view($this->view_path.'.common.attribute_options', compact('attributes'))->render();
        return response()->json(json_encode($response));

    }
    public function loadAttributeRow()
    {
        $response = [];
        $data = [];
        $data['attribute_groups'] = AttributeGroup::active()->orderBy('title')->get();
        $data['attributes'] = $data['attribute_groups']->first()->attributes()->orderBy('title')->get();
        $response['html'] = view($this->view_path.'.common.attribute_row', compact('data'))->render();
        return response()->json(json_encode($response));
    }

    public function loadSlug(Request $request)
    {
        $response = [];
        $response['error'] = true;
        if(Product::where('title', $request->get('title'))->count() > 0){
            $response['message'] = '<strong>'.$request->get('title').'</strong>'.'&nbsp; title already exist.';
        }else{
            $response['slug'] = Str::slug($request->get('title', '-'));
            $response['error'] = false;

        }
        return response()->json(json_encode($response));
    }

    public function store(addProductvalidation $request)
    {

        if($request->hasFile('image')){
            $file = $request->file('image');
            $file_name = parent::getRandormImageName().'_'.$file->getClientOriginalName();

            //folder exist
            $this->checkFolderExist();
            $file->move(public_path($this->folder_path), $file_name);

            // upload thumb image
            $image_thumb_config = config('cms.product.image-dimentions.main-image');
            foreach ($image_thumb_config as $thumb_config) {
                $thumb_image = Image::make(($this->folder_path).DIRECTORY_SEPARATOR.$file_name);
                $thumb_image->resize($thumb_config['width'], $thumb_config['height']);
                $thumb_image->save(public_path($this->folder_path).DIRECTORY_SEPARATOR.$thumb_config['width'].'_'.$thumb_config['height'].'_'.$file_name);
            }

            $request->request->add(['main_image' => $file_name]);
        }

        $product = $this->model->create($request->all());
        $product->tags()->sync($request->get('tags'));


        //store the attributes
        $pivot_data = [];
        foreach ($request->get('attribute') as $key => $attribute_id){
            $pivot_data[$attribute_id] = [
                'content'  => $request->get('attribute_value')[$key],
            ];
        }
        $product->attributes()->sync($pivot_data);

        //upload galleries
        foreach ($request->file('gallery_image') as $key => $g_image){

            $file = $g_image;
            $file_name = parent::getRandormImageName().'_'.$file->getClientOriginalName();

            //folder exist
            $this->checkFolderExist();
            $file->move(public_path($this->folder_path), $file_name);

            // upload thumb image
            $image_thumb_config = config('cms.product.image-dimentions.gallery-image');
            foreach ($image_thumb_config as $thumb_config) {
                $thumb_image = Image::make(($this->folder_path).DIRECTORY_SEPARATOR.$file_name);
                $thumb_image->resize($thumb_config['width'], $thumb_config['height']);
                $thumb_image->save(public_path($this->folder_path).DIRECTORY_SEPARATOR.$thumb_config['width'].'_'.$thumb_config['height'].'_'.$file_name);
            }

            ProductImage::create([
               'product_id' => $product->id,
               'image'      => $file_name,
               'alt_text'   => $request->get('gallery_alt_text')[$key],
               'caption'    => $request->get('gallery_caption')[$key],
               'rank'       => $key + 1,
               'status'     => $request->get('gallery_status')[$key],
            ]);
        }

        $request->session()->flash('success_message', $this->panel. ' Added Successfully');
        return redirect()->route($this->base_route);
    }

    public function show($id)
    {
        $data = [];
        $data['row'] = Category::find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = Category::find($id);

        if(!$data['row']){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        $data['parent_categories'] = $this->getParentCategoryForSelect();

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        $row = Category::find($id);
        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        //remove old image if checkbox is checked
        if($request->has('remove_image')) {
            if($row->image){
                if (File::exists(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image))) {
                    File::delete(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image));
                }
                $row->image = null;
                $row->save();
            }
        }
       if ($request->hasFile('banner_image')) {
            $file = $request->file('banner_image');
            $file_name = parent::getRandormImageName() . '_' . $file->getClientOriginalName();

            //folder exist
            $this->checkFolderExist();
            $file->move(public_path($this->folder_path), $file_name);
            $request->request->add(['image' => $file_name]);

            //Remove old image
            if (File::exists(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image))) {
                File::delete(public_path($this->folder_path . DIRECTORY_SEPARATOR . $row->image));
            }
        }


        $row->update($request->all());

        $request->session()->flash('success_message', $this->panel . ' Updated Successfully');
        return redirect()->route($this->base_route);

    }

    public function destroy(Request $request, $id)
    {
        $row = Category::find($id);

        if(!$row){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        //remove the image
        if(File::exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image))){
            File::delete(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image));
        }
        $row->delete();
        $request->session()->flash('error_message', $this->panel.'Data Deleted Successfully');
        return redirect()->route($this->base_route);
    }

    protected function getParentCategoryForSelect(){
        $data = [];
        $data['parent_categories'] = Category::where('parent_id', 0)->pluck('title', 'id')->toArray();
        //$data['parent_categories'] = array_merge([0 => '- Is Parent -'], $data['parent_categories']);

        return [0 => '- Is Parent -'] + $data['parent_categories'];
    }

    public function getCategoryList()
    {
        $categories = Category::active()->select('id','title', 'parent_id')->get();
        foreach ($categories as $row){

            if($row->parent_id !==0){
                $parent = Category::find($row->parent_id);
                $row->title = $parent->title .'>>' . $row->title;
            }
        }

        //dd($categories);
        return $categories;
    }


}
