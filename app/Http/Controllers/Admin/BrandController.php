<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use File, Str;
use App\Models\Tag;
use Illuminate\Http\Request;

class BrandController extends AdminBaseController
{
    protected $base_route= 'admin.brand';
    protected $view_path = 'admin.brand';
    protected $panel = 'Brand';



    public function index()
    {
        $data = [];
        $data['rows'] = Brand::select('id','title', 'slug', 'status', 'created_at')
            ->orderBy('id', 'desc')
            ->paginate(5);

        //dd($data['rows']);

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'))->with('no', 1);
    }

    public function create()
    {

        return view(parent::loadCommonDataToView($this->view_path.'.create'));

    }

    public function store(Request $request)
    {
        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        Brand::create($request->all());
        $request->session()->flash('success_message', $this->panel. ' Added Successfully');
        return redirect()->route($this->base_route);
    }

    public function show($id)
    {
        $data = [];
        $data['row'] = Brand::find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = Brand::find($id);

        if(!$data['row']){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function update(Request $request, $id)
    {
        //dd($request->all());
        $row = Brand::find($id);

        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        $row->update($request->all());

        $request->session()->flash('success_message', $this->panel . ' Updated Successfully');
        return redirect()->route($this->base_route);

    }

    public function destroy(Request $request, $id)
    {
        $row = Brand::find($id);

        if(!$row){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        $row->delete();
        $request->session()->flash('error_message', $this->panel.'Data Deleted Successfully');
        return redirect()->route($this->base_route);
    }
}
