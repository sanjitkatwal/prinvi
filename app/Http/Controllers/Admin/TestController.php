<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\AddFormValidation;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class TestController extends AdminBaseController
{
    protected $base_route= 'admin.test';
    protected $view_path = 'admin.test';
    protected $panel = 'Test';
    protected $folder = 'test';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = 'images'.DIRECTORY_SEPARATOR.$this->folder;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = Category::select('id','title', 'parent_id', 'slug', 'description', 'image','created_at')
            ->orderBy('id', 'desc')
            ->paginate(5);

        foreach ($data['rows'] as $row){
            if ($row->parent_id == 0){
                $row->parent = '';
            }else{
                $parent = Category::find($row->parent_id);
                $row->parent = $parent->title;
            }
        }

        //dd($data['rows']);

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'));
    }

    public function create()
    {
        $data = [];
        $data['parent_categories'] = Category::select('id', 'title', 'parent_id')
            ->where('parent_id', 0)->get();
        return view(parent::loadCommonDataToView($this->view_path.'.create'), compact('data'));

    }

    public function store(AddFormValidation $request)
    {
        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        if($request->hasFile('banner_image')){
            $file = $request->file('banner_image');
            $file_name = rand(2000,9999).'-'.$file->getClientOriginalName();

            //folder exist
            if(!file_exists(public_path($this->folder_path))){
                mkdir(public_path($this->folder_path));
            }
            $file->move(public_path($this->folder_path), $file_name);

            $request->request->add(['image' => $file_name]);
        }
        Category::create($request->all());
        $request->session()->flash('success_message', $this->panel. ' Added Successfully');
        return redirect()->route($this->base_route);
    }

    public function show($id)
    {
        $data = [];
        $data['row'] = Category::find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
    }

    public function edit($id)
    {
        $data = [];
        $data['parent_categories'] = Category::select('id', 'title', 'parent_id')
            ->where('parent_id', 0)->get();
        $data['row'] = Category::find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Request $request, $id)
    {
        $row = Category::find($id);

        if(!$row){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        //remove the image
        if(file_exists(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image))){
            unlink(public_path($this->folder_path.DIRECTORY_SEPARATOR.$row->image));
        }
        $row->delete();
        $request->session()->flash('error_message', $this->panel.'Data Deleted Successfully');
        return redirect()->route($this->base_route);
    }
}
