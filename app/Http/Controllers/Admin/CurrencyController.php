<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\Currency\currencyAddValidation;
use App\Http\Requests\Currency\currencyEditValidation;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Currency;
use File;

class CurrencyController extends AdminBaseController
{
    protected $base_route= 'admin.currency';
    protected $view_path = 'admin.currency';
    protected $panel = 'Currency';



    public function __construct(Currency $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id','is_default', 'title', 'slug',
            'symbol', 'status', 'rank', 'rate', 'created_at')
            ->orderBy('rank')
            ->paginate(5);

        //dd($data['rows']);

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'))->with('no', 1);
    }

    public function create()
    {
        return view(parent::loadCommonDataToView($this->view_path.'.create'));

    }

    public function store(currencyAddValidation $request)
    {

        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        $this->model->create($request->all());
        $request->session()->flash('success_message', $this->panel. ' Added Successfully');
        return redirect()->route($this->base_route);
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);
        parent::rowExist($data['row']);

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function update(currencyEditValidation $request, $id)
    {
        $row = $this->model->find($id);

        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);
        $row->update($request->all());

        $request->session()->flash('success_message', $this->panel . ' Updated Successfully');
        return redirect()->route($this->base_route);

    }

    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);

        parent::rowExist($row);
        if(!$row->isDeleteble()){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        $row->delete();
        $request->session()->flash('success_message', $this->panel.'Data Deleted Successfully');
        return redirect()->route($this->base_route);
    }

    public function reorderCurrency(Request $request)
    {
        if(!$request->has('id')){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        foreach ($request->get('id') as $key => $id) {
            $row = $this->model->find($id);
            $row->rank = $key +1;
            $row->save();
        }

        $request->session()->flash('success_message', $this->panel.' Sorted Successfully');
        return redirect()->route($this->base_route);
    }

}
