<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AttributeGroup\AttributeGroupAddValidation;
use App\Http\Requests\AttributeGroup\AttributeGroupEditValidation;
use App\Models\Attribute;
use App\Models\AttributeGroup;
use App\Models\Page;
use File, Str;
use App\Models\Tag;
use Illuminate\Http\Request;

class AttributeGroupController extends AdminBaseController
{
    protected $base_route= 'admin.attribute-group';
    protected $view_path = 'admin.attribute-group';
    protected $panel = 'Attribute Group';


    public function __construct(AttributeGroup $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $data = [];
        $data['rows'] = $this->model->select('id','title', 'slug', 'status')
            ->orderBy('id', 'desc')
            ->paginate(5);

        //dd($data['rows']);

        return view(parent::loadCommonDataToView($this->view_path.'.index'), compact('data'))->with('no', 1);
    }

    public function create()
    {

        return view(parent::loadCommonDataToView($this->view_path.'.create'));

    }

    public function store(AttributeGroupAddValidation $request)
    {
        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        $this->model->create($request->all());
        $request->session()->flash('success_message', $this->panel. ' Added Successfully');
        return redirect()->route($this->base_route);
    }

    public function show($id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);

        return view(parent::loadCommonDataToView($this->view_path.'.show'), compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $data = [];
        $data['row'] = $this->model->find($id);

        if(!$data['row']){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        return view(parent::loadCommonDataToView($this->view_path.'.edit'), compact('data'));
    }

    public function update(AttributeGroupEditValidation $request, $id)
    {
        //dd($request->all());
        $row = $this->model->find($id);

        $request->request->add(['slug' => Str::slug($request->get('title', '-'))]);

        $row->update($request->all());

        $request->session()->flash('success_message', $this->panel . ' Updated Successfully');
        return redirect()->route($this->base_route);

    }

    public function destroy(Request $request, $id)
    {
        $row = $this->model->find($id);

        if(!$row){
            $request->session()->flash('error_message', 'Invalid request');
            return redirect()->route($this->base_route);
        }

        $row->delete();
        $request->session()->flash('error_message', $this->panel.'Data Deleted Successfully');
        return redirect()->route($this->base_route);
    }
}
