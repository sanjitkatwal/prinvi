<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;

class UserController extends AdminBaseController
{
    protected $base_route= 'admin.user';
    protected $view_path = 'admin.user';
    protected $panel = 'User';
    protected $folder = 'user';
    protected $folder_path;    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $data['rows'] = User::select('id','name', 'email', 'first_name', 'middle_name', 'last_name', 'created_at')
            ->orderBy('id', 'desc')
            ->paginate(20);

        return view('admin.user.index',compact('data'));
    }

    public function profile()
    {
        $data = [];
        $data['user']  = auth()->user();
        return view('admin.user.profile', compact('data'));
    }

    public function profileUpdate(Request $request)
    {
        //dd($request->all());

        $this->validateLogin($request);
        $user = auth()->user();
        $user->name         = $request->get('name');
        $user->email        = $request->get('email');
        $user->first_name   = $request->get('first_name');
        $user->middle_name  = $request->get('middle_name');
        $user->last_name    = $request->get('last_name');
        $user->contact      = $request->get('contact');
        $user->address      = $request->get('address');

        if($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }
        $user->save();

        $request->session()->flash('success_message', 'Profile Updated Successfully.');


        return redirect()->route('admin.profile');
    }

    protected function validateLogin($request){
        /*$request->validate([
            'name' => 'required|string',
            'name' => 'required|string',
            'password' => 'required|string',
        ]);*/

        $rules = [
            'name'          => 'required|string|unique:users,name,'.auth()->user()->id,
            'email'         => 'required|string|unique:users,email,'.auth()->user()->id,
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'contact'       => 'integer',
            'address'       => 'string',
        ];

        if($request->get('password')){
            $rules['password'] = 'required|string|min:6|confirmed';
        }

        $this->validate($request, $rules);
    }

    public function store(Request $request)
    {
        //
    }
    public function show($id)
    {
        $data = [];
        $data['row']  = User::find($id);
        if(!$data['row'])
            return redirect()->route('admin.user');

        return view(parent::loadCommonDataToView('admin.user.show'), compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
