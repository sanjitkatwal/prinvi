<?php

namespace App\Http\Requests\Category;

use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class categoryEditValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customValidation();
        return [
            'title'=> 'required | max:100 | id_validation | unique:categories,title,'.request()->get('id'),
            'description'   => 'required',
        ];
    }

    public function messages()
    {
        return [
           // 'title.required' => 'Please insert the data.',
            'title.id_validation' => 'Invalid id is passed.',
        ];
    }

    public function customValidation()
    {
        Validator::extend('id_validation', function($attrubute, $value, $parameters, $validator){

            $id = request()->get('id');
            if(Category::find($id))
                return true;

            return false;
        });
    }
}
