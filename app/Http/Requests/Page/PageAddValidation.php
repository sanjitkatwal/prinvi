<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class PageAddValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $this->customValidation();
        return [
            //'title'     =>'required | unique:pages, title',
            'page_type'     => 'link_page_validation',
            'status'         => 'required',
            'title' => 'required',
        ];
    }

    public function messages()
    {
        return [

            'page_type.link_page_validation' => 'Link is required.',
        ];
    }

    public function customValidation()
    {
        Validator::extend('link_page_validation', function ($attribute, $value, $parameters, $validator){
            if ($value == 'link_page'){
                if (!$this->request->get('link'))
                    return false;
                return true;
            }
            return true;
        });
    }
}
