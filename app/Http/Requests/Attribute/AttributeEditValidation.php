<?php

namespace App\Http\Requests\Attribute;

use App\Models\Attribute;
use App\Models\AttributeGroup;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class AttributeEditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customValidation();
        return [
            'title'                 =>'required | max:100 | id_validation | unique:attributes,title,'.request()->get('id'),
            'attribute_group_id'    =>'required | group_id_validation',
            'status'                => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.unique'                          => 'This title is already taken.',
            'title.id_validation'                   => 'Invalid attribute ID passed.',
            'attribute_group_id.group_id_validation' => 'Invalid group ID passed.',
        ];
    }

    public function customValidation()
    {
        Validator::extend('id_validation', function ($attribute, $value, $parameters, $validator){
            $id = request()->get('id');
            if(Attribute::find($id))
                return true;
            return false;
        });

        Validator::extend('group_id_validation', function ($attribute, $value, $parameters, $validator){
            if (AttributeGroup::find($value))
                return true;
            return false;
        });
    }
}
