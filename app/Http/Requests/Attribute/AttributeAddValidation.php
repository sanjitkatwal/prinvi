<?php

namespace App\Http\Requests\Attribute;

use App\Models\AttributeGroup;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class AttributeAddValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $this->customValidation();
        return [
            'title'                 =>'required | unique:attributes',
            'attribute_group_id'    =>'required |group_id_validation',
            'status'                => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.unique'                          => 'This title is already taken.',
            'attribute_group_id.group_id_validation' => 'Invalid group ID passed.',
        ];
    }

    public function customValidation()
    {
        Validator::extend('group_id_validation', function ($attribute, $value, $parameters, $validator){
            if (AttributeGroup::find($value))
                return true;
            return false;
        });
    }
}
