<?php

namespace App\Http\Requests\Menu;

use App\Models\Menu;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class MenuEditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $this->customValidation();
        return [
            'title'=> 'required | max:100 | id_validation | unique:menu_section,title,'.request()->get('id'),
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please insert the Title.',
            'title.id_validation' => 'Invalid id is passed.',
        ];
    }

    public function customValidation()
    {
        Validator::extend('id_validation', function($attrubute, $value, $parameters, $validator){

            $id = request()->get('id');
            if(Menu::find($id))
                return true;

            return false;
        });
    }

}
