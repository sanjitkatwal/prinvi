<?php

namespace App\Http\Requests\Product;


use App\Models\Brand;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class editProductvalidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customValidation();
        return [
            'category_id'       => 'required | cat_id_validation',
            'title'             => 'required | max:150',
            'slug'              => 'required | max:150 | unique:products,slug,'.$this->request->get('id'),
            'new_price'         => 'required | numeric | compare_price',
            'quantity'          => 'required | numeric',
            'brand_id'          => 'required | brand_id_validation',
            'image'             => 'image',
        ];
    }

    public function messages()
    {
        return [
            'category_id.cat_id_validation' => 'Invalid Category.',
            'new_price.compare_price'       => 'Old price must be equall to or greator then new price.',
        ];
    }

    public function customValidation()
    {
        Validator::extend('cat_id_validation', function($attrubute, $value, $parameters, $validator){

            if(Category::find($value))
                return true;

            return false;
        });

        Validator::extend('compare_price', function($attrubute, $value, $parameters, $validator){

            $old_price = $this->request->get('old_price');
            if(!$old_price)
                return true;

            if($value > $old_price){
                return false;
            }elseif($value == $old_price){
                return true;
            }else{
                return true;
            }
        });

        Validator::extend('brand_id_validation', function($attrubute, $value, $parameters, $validator){

            if(Brand::find($value))
                return true;

            return false;
        });
    }
}
