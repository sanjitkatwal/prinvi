<?php

namespace App\Http\Requests\Currency;

use App\Models\Currency;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class currencyEditValidation extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customValidation();
        return [
            'title'     => 'required | max:100 | unique:currencies,title,'.$this->request->get('id'). ' | id_validation',
            'symbol'    => 'required',
            'rate'      => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.unique'        => 'This title has already taken, choose unique title.',
            'title.id_validation' => 'Invalid id is passed.',
        ];
    }

    public function customValidation()
    {
        Validator::extend('id_validation', function($attrubute, $value, $parameters, $validator){

            $id = request()->get('id');
            if(Currency::find($id))
                return true;

            return false;
        });
    }
}
