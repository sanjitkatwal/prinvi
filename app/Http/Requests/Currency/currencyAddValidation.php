<?php

namespace App\Http\Requests\Currency;

use Illuminate\Foundation\Http\FormRequest;

class currencyAddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required | max:100 | unique:currencies,title',
            'rate'         => 'required',
            'symbol'       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'    =>'This fields need data.',
            'title.unique'      =>'This title already taken, choose another.'
        ];
    }
}
