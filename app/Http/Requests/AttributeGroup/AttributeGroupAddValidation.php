<?php

namespace App\Http\Requests\AttributeGroup;

use Illuminate\Foundation\Http\FormRequest;

class AttributeGroupAddValidation extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title'     =>'required | unique:attribute_groups',
            'status'    => 'required',
        ];
    }

    public function messages()
    {
        return [

            'title.unique' => 'This title is already taken',
        ];
    }
}
