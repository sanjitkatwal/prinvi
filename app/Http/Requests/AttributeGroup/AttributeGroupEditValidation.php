<?php

namespace App\Http\Requests\AttributeGroup;

use App\Models\AttributeGroup;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class AttributeGroupEditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->customValidation();
        return [
            'title'=> 'required | max:100 | id_validation | unique:attribute_groups,title,'.request()->get('id'),
            'status'   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.id_validation' => 'Invalid id is passed.',
        ];
    }

    public function customValidation()
    {
        Validator::extend('id_validation', function($attrubute, $value, $parameters, $validator){

            $id = request()->get('id');
            if(AttributeGroup::find($id))
                return true;

            return false;
        });
    }
}
